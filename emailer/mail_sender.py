__author__ = 'dimitriy'

from amqp_core.worker import AMQPWorker
from emailer.defaults import MAIL_QUEUE_NAME
from emailer.queue_msg import QMailMessage
from utils.json_helper import deserialize


class MailSender:
    def __init__(self, smtp, loop):
        self._smtp = smtp
        self._loop = loop

    async def send(self, message):
        if message is not None:
            res = await self._smtp.send_message(message)
            return res


class MailQueueWorker(AMQPWorker):
    sender_class = MailSender

    def __init__(self, smtp, template_env=None, transport=None, protocol=None, loop=None):
        super(MailQueueWorker, self).__init__(MAIL_QUEUE_NAME, transport, protocol, loop)
        self._smtp = smtp
        self._template_env = template_env

    async def handler(self, channel, body, enveloppe, properties):
        # handle queue task
        data = deserialize(body)
        message = QMailMessage(self._template_env, **data)
        mime = message.get_mime()
        if mime is not None:
            sender = self.sender_class(self._smtp, self._loop)
            await sender.send(mime)
            await super(MailQueueWorker, self).handler(channel, body, enveloppe, properties)

