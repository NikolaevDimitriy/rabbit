__author__ = 'dimitriy'

import asyncio
import jinja2
from emailer import aiosmtplib
from emailer.mail_sender import MailQueueWorker
from emailer.defaults import MAIL_QUEUE_NAME
from jinja2 import PrefixLoader, PackageLoader
from babel.support import Translations



async def init(loop):
    smtp = aiosmtplib.SMTP(hostname='localhost', port=1025, loop=loop)
    jenv = jinja2.Environment(loader=PackageLoader('emailer'), extensions=['jinja2.ext.i18n'])
    jenv.install_gettext_translations(Translations())
    snd = MailQueueWorker(smtp,
                          template_env=jenv,
                          loop=loop)
    await snd.connect()
    await snd.subscribe()
    return snd

eloop = asyncio.get_event_loop()
sender = eloop.run_until_complete(init(eloop))

try:
    print('run email worker')
    eloop.run_forever()
except KeyboardInterrupt:
    pass