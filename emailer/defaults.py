__author__ = 'dimitriy'

from enum import IntEnum


class MailType(IntEnum):
    plain = 1
    html = 2

MAIL_QUEUE_NAME = "sending_email_queue"

TEMPLATE_INDEX = {
    'register': 'registration.html'
}


