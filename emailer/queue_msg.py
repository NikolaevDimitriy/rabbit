__author__ = 'dimitriy'
import jinja2
import settings as ls
from email.mime.text import MIMEText
from email.header import Header
from amqp_core.helpers import serialize
from emailer.defaults import MailType


class QMailMessage:

    def __init__(self, template_env=None, **kwargs):
        self._template_env = template_env

        self._mtype = kwargs.get('mtype', None)
        self._ad_from = kwargs.get('ad_from', ls.SENDER_EMAIL_ADDRESS)
        self._ad_to = kwargs.get('ad_to', None)
        self._subject = kwargs.get('subject', '')
        self._message = kwargs.get('message', None)
        self._context = kwargs.get('context', None)
        self._template_name = kwargs.get('template_name', None)
        self._attachments = kwargs.get('attachments', [])

    def get_mime(self):
        m_type = 'html' if self._mtype == MailType.html else 'plain'
        text = self.get_text()
        if text is None:
            return None
        msg = MIMEText(text, m_type, 'utf-8')
        msg['From'] = self._ad_from
        msg['To'] = self._ad_to
        msg['Subject'] = Header(self._subject, charset='utf-8')
        return msg

    def get_text(self):
        text = self._message
        if self._template_name and self._template_env is not None:
            try:
                template = self._template_env.get_template(self._template_name)
                text = template.render(self._context)
            except jinja2.TemplateNotFound as e:
                # log
                return None
            except Exception as x:
                return None
        return text

    def to_dict(self):
        return {
            'mtype': int(self._mtype),
            'ad_from': self._ad_from,
            'ad_to': self._ad_to,
            'subject': self._subject,
            'message': self._message,
            'context': self._context,
            'template_name': self._template_name,
            'attachments': [x.to_dict() for x in self._attachments]
        }

    # def from_dict(self, **kwargs):
    #     self._mtype = kwargs.get('mtype', None)
    #     self._ad_from = kwargs.get('ad_from', None)
    #     self._ad_to = kwargs.get('ad_to', None)
    #     self._subject = kwargs.get('subject', '')
    #     self._message = kwargs.get('message', '')
    #     self._context = kwargs.get('context', None)
    #     self._template_name = kwargs.get('template_name', None)
    #     self._attachments = [QMailMessageAttachment().from_dict(**attach) for attach in kwargs['attachments']]
    #     return self

    def get_package(self):
        return serialize(self)  # only ascii


class QMailMessageAttachment:

    def from_dict(self, **kwargs):
        return self

    def to_dict(self):
        return {}