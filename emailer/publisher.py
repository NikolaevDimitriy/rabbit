__author__ = 'dimitriy'

from amqp_core.producer import AMQPProducer
import settings as ls
from emailer.defaults import MAIL_QUEUE_NAME
from emailer.queue_msg import QMailMessage


class EmailPublisher(AMQPProducer):

    def __init__(self, transport=None, protocol=None, loop=None):
        super(EmailPublisher, self).__init__(MAIL_QUEUE_NAME, transport, protocol, loop)

    async def send_message(self, mtype, mail_from=ls.SENDER_EMAIL_ADDRESS, mail_to=None,
                           subject='', message='',
                           context=None, template_name=None,
                           attachments=()):
        msg = QMailMessage(mtype=mtype, ad_from=mail_from, ad_to=mail_to,
                           subject=subject, message=message,
                           context=context, template_name=template_name,
                           attachments=attachments).get_package()
        await self.publish(msg)