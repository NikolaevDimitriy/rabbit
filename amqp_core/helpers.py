__author__ = 'dimitriy'

import ujson as json


def serialize(instance):
    return json.dumps(instance.to_dict())
