__author__ = 'dimitriy'
import abc
from amqp_core.protocol import AioAMQPProtocol


class AMQPProducer(AioAMQPProtocol, metaclass=abc.ABCMeta):

    def __init__(self, queue_name, transport=None, protocol=None, loop=None):
        super(AMQPProducer, self).__init__(transport, protocol, loop)
        self._queue_name = queue_name

    async def declare_queue(self):
        await self._channel.queue_declare(self._queue_name, durable=True)

    async def publish(self, message):
        await self._channel.basic_publish(
                payload=message,
                exchange_name='',
                routing_key=self._queue_name,
                properties={
                    'delivery_mode': 2,
                },
        )


