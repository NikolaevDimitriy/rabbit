__author__ = 'dimitriy'

import abc
from amqp_core.protocol import AioAMQPProtocol


class AMQPWorker(AioAMQPProtocol, metaclass=abc.ABCMeta):

    def __init__(self, queue_name, transport=None, protocol=None, loop=None):
        super(AMQPWorker, self).__init__(transport, protocol, loop)
        self._queue_name = queue_name

    async def subscribe(self):
        await self._channel.queue_declare(queue_name=self._queue_name, durable=True)
        await self._channel.basic_qos(prefetch_count=5, prefetch_size=0, connection_global=False)
        await self._channel.basic_consume(self.handler, queue_name=self._queue_name)

    @abc.abstractmethod  # have to contain inner coroutine
    async def handler(self, channel, body, enveloppe, properties):
        await channel.basic_client_ack(delivery_tag=enveloppe.delivery_tag)
