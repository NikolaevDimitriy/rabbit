__author__ = 'dimitriy'
import abc
import asyncio
import aioamqp

async def connect(loop):
    try:
        transport, protocol = await aioamqp.connect(loop=loop)
        return transport, protocol
    except aioamqp.AmqpClosedConnection:
        print("closed connections")
        return None, None


class AioAMQPProtocol(dict, metaclass=abc.ABCMeta):

    def __init__(self, transport=None, protocol=None, loop=None):
        self._loop = loop
        self._transport = transport
        self._protocol = protocol
        self._channel = None
        if self._loop is None:
            self._loop = asyncio.get_event_loop()

    async def connect(self):
        if self._transport is None or self._protocol is None:
            self._transport, self._protocol = await connect(self._loop)
        if self._transport is not None and self._protocol is not None:
            self._channel = await self._protocol.channel()
            return True
        return False
