__author__ = 'dimitriy'

from settings import local_settings

REGISTRATION_APPROVAL_PERIOD = 86400

HOST = 'localhost'
PORT = 8080

DEBUG = local_settings.LOCAL_DEBUG

SENDER_EMAIL_ADDRESS = 'from@mail.mail'

CURRENT_DIR = local_settings.LOCAL_CURRENT_DIR

DB_MASTER_HOST = local_settings.LOCAL_DB_MASTER_HOST
DB_MASTER_NAME = local_settings.LOCAL_DB_MASTER_NAME
BD_MASTER_USER = local_settings.LOCAL_BD_MASTER_USER
BD_MASTER_PWD = local_settings.LOCAL_BD_MASTER_PWD

DB_SLAVE_HOST = local_settings.LOCAL_DB_SLAVE_HOST
DB_SLAVE_NAME = local_settings.LOCAL_DB_SLAVE_NAME
BD_SLAVE_USER = local_settings.LOCAL_BD_SLAVE_USER
BD_SLAVE_PWD = local_settings.LOCAL_BD_SLAVE_PWD


SECRET_KEY = local_settings.LOCAL_SECRET_KEY

SESSION_COOKIE_AGE = 3600  # 1 hour session timeout
SESSION_REDIS_HOST = local_settings.LOCAL_SESSION_REDIS_HOST
SESSION_REDIS_PORT = local_settings.LOCAL_SESSION_REDIS_PORT
SESSION_REDIS_DB = local_settings.LOCAL_SESSION_REDIS_DB
SESSION_REDIS_PASSWORD = local_settings.LOCAL_SESSION_REDIS_PASSWORD
SESSION_REDIS_PREFIX = local_settings.LOCAL_SESSION_REDIS_PREFIX
# if you prefer domain socket connection
# you can just add this line instead of SESSION_REDIS_HOST and SESSION_REDIS_PORT
#LOCAL_SESSION_REDIS_UNIX_DOMAIN_SOCKET_PATH = '/var/run/redis/redis.sock'
# you can also use redis from url
SESSION_REDIS_URL = local_settings.LOCAL_SESSION_REDIS_URL

# also available setup connection via redis.ConnectionPool like
SESSION_SERIALIZER = local_settings.LOCAL_SESSION_SERIALIZER
SESSION_REDIS_JSON_ENCODING = local_settings.LOCAL_SESSION_REDIS_JSON_ENCODING

SEARCH_ENGINE_HOST = local_settings.LOCAL_SEARCH_ENGINE_HOST
SEARCH_ENGINE_PORT = local_settings.LOCAL_SEARCH_ENGINE_PORT


DEFAULT_AVATAR = '/static/img/avatar.png'
