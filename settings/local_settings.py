# -*- coding: utf-8 -*-
import os
__author__ = 'dimitriy'
LOCAL_DEBUG = True

LOCAL_DB_MASTER_HOST = '127.0.0.1'
LOCAL_DB_MASTER_NAME = 'rabbit'
LOCAL_BD_MASTER_USER = 'postgres'
LOCAL_BD_MASTER_PWD = 'sps67'

LOCAL_DB_SLAVE_HOST = '127.0.0.1'
LOCAL_DB_SLAVE_NAME = 'rabbit'
LOCAL_BD_SLAVE_USER = 'postgres'
LOCAL_BD_SLAVE_PWD = 'sps67'

LOCAL_CURRENT_DIR = os.getcwd()

LOCAL_SECRET_KEY = '*%k$fx390z=*xfm4!e*t)$(z+4m!p*m2i%@qemqx!%3v4-t#he'

ADMIN_PASSWORD_PREFIX = 'sps'

LOCAL_SESSION_ENGINE = 'redis_sessions_fork.session'
LOCAL_SESSION_REDIS_HOST = '127.0.0.1'
LOCAL_SESSION_REDIS_PORT = 6379
LOCAL_SESSION_REDIS_DB = 2
LOCAL_SESSION_REDIS_PASSWORD = None
LOCAL_SESSION_REDIS_PREFIX = 'rabbit'
# if you prefer domain socket connection
# you can just add this line instead of SESSION_REDIS_HOST and SESSION_REDIS_PORT
#LOCAL_SESSION_REDIS_UNIX_DOMAIN_SOCKET_PATH = '/var/run/redis/redis.sock'
# you can also use redis from url
LOCAL_SESSION_REDIS_URL = 'redis://127.0.0.1:6379/2'

# also available setup connection via redis.ConnectionPool like
LOCAL_SESSION_REDIS_CONNECTION_POOL = 'nosql_provider.redis_adapter.pool_session'

LOCAL_SESSION_SERIALIZER = 'redis_sessions_fork.serializers.UjsonSerializer'

LOCAL_SESSION_REDIS_JSON_ENCODING = 'utf8'

LOCAL_SEARCH_ENGINE_HOST = 'localhost'
LOCAL_SEARCH_ENGINE_PORT = 9306

