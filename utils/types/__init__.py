__author__ = 'dimitriy'
from collections import namedtuple


class KeyValue(namedtuple('KeyValue', 'key value')):
    def to_dict(self):
        return {
            'key': self.key,
            'value': str(self.value),
        }
    [...]