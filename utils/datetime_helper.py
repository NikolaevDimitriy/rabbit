__author__ = 'dimitriy'

import datetime
from dateutil.tz import tzlocal, tzutc
from dateutil.parser import parse
import pytz


tzlocal = tzlocal()
tzutc = tzutc()

# dd.mm.yyyy pattern
date_ru_pattern = r'^(0[1-9]|[1-2][0-9]|31(?!\.(?:0[2469]|11))|30(?!\.02))\.(0[1-9]|1[0-2])\.([12]\d{3})$'


def get_utc_now():
    now = datetime.datetime.now(tzlocal)
    utc = now.astimezone(tzutc)
    return utc


def get_local_now():
    now = datetime.datetime.now(tzlocal)
    return now


def date_to_str_safe(value, fmt="%d.%m.%Y %H:%M:%S.%f"):
    return value.strftime(fmt) if value else ''


def date_parse(value, default=None):
    res = parse(value, dayfirst=True, default=datetime.datetime.min)
    if res == datetime.datetime.min:
        if default:
            return default
        raise ValueError
    return res


def get_tz_offset_str(tz):
    offset = int(pytz.timezone(tz).utcoffset(datetime.datetime.utcnow()).total_seconds()/60/60)
    if offset > 0:
        return '+%d' % (offset,)
    return str(offset)