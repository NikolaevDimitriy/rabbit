__author__ = 'dimitriy'
import time


def ignore_exception(ignore_exception=Exception, default_val=None):
    """ Decorator for ignoring exception from a function
    e.g.   @ignore_exception(DivideByZero)
    e.g.2. ignore_exception(DivideByZero)(Divide)(2/0)
    """
    def dec(function):
        def _dec(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except ignore_exception:
                return default_val
        return _dec
    return dec


def benchmark(func):

    def wrapper(*args, **kwargs):
        t = time.clock()
        res = func(*args, **kwargs)
        print(func.__name__, time.clock() - t)
        return res
    return wrapper


def async_benchmark(func):

    async def wrapper(*args, **kwargs):
        t = time.clock()
        res = await func(*args, **kwargs)
        print(func.__name__, time.clock() - t)
        return res
    return wrapper


class classonlymethod(classmethod):
    def __get__(self, instance, owner):
        if instance is not None:
            raise AttributeError("This method is available only on the class, not on instances.")
        return super(classonlymethod, self).__get__(instance, owner)
