__author__ = 'dimitriy'

from utils import flags_helper


def is_flag_contains_filter(src):
    def is_flag_contains_func(pair):
        return flags_helper.is_contains(src, pair.key)
    return is_flag_contains_func


def is_flag_contains(src, pair):
    return flags_helper.is_contains(src, pair.key)


def get_key_value_model(item):
    return {
        'key': item.key,
        'value': str(item.value),
    }