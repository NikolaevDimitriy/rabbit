__author__ = 'dimitriy'

import ujson as json


def serialize_list(converter, lst, total=None):
    source = [converter(n) for n in lst]
    data = json.dumps(source, ensure_ascii=False)
    return u"{'data': %s, 'total': %d}" % (data, total if total else len(source),)


def serialize_entity_list(entity_list, total=None, **kwargs):
    source = [n.to_dict(**kwargs) for n in entity_list]
    data = json.dumps(source, ensure_ascii=False)
    return u"{'data': %s, 'total': %d}" % (data, total if total else len(source),)


def serialize_entity(instance, **kwargs):
    return json.dumps(instance.to_dict(**kwargs), ensure_ascii=False)


def serialize(instance):
    return json.dumps(instance, ensure_ascii=False)


def deserialize_list(string):
    return json.loads(string)


def deserialize(string):
    return json.loads(string)
