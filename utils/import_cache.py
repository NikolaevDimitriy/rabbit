__author__ = 'dimitriy'

import importlib


class ModelCache(object):
    _models_cache = {}

    def get_model_class(self, module_name, class_name):
        key = module_name+class_name
        if key in self._models_cache:
            return self._models_cache[key]
        module = importlib.import_module(module_name)
        try:
            self._models_cache[key] = getattr(module, class_name)
            return self._models_cache[key]
        except Exception as x:
            return None


model_cache = ModelCache()