__author__ = 'dimitriy'


def is_contains(src, flag):
    return (src & flag) == flag


def set_flag(src, flag):
    if not is_contains(src, flag):
        src |= flag
    return src


def remove_flag(src, flag):
    if is_contains(src, flag):
        src ^= flag
    return src


def get_flags_from_list(lst: list):
    res = 0
    for val in lst:
        res |= val.value
    return res
