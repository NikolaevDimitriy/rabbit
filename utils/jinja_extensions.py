__author__ = 'dimitriy'
from utils.flags_helper import is_contains
import babel


def empty_if_none(str):
    return str if str is not None else ''


def zero_if_none(val):
    return val if val is not None else 0


def check_flag(val, fl):
    if val is not None:
        return 'true' if is_contains(val, fl) else 'false'
    return 'false'


def format_datetime(value, format='Y/m/d'):
    if value is not None:
        return babel.dates.format_datetime(value, format)
    return ''
