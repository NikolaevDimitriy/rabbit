__author__ = 'dimitriy'
from gettext import gettext, ngettext, dgettext, dngettext


def gettext_local(msg):
    return gettext(msg)


def ngettext_local(msg):
    return ngettext(msg)


def dgettext_local(msg):
    return dgettext(msg)


def dngettext_local(msg):
    return dngettext(msg)
