__author__ = 'dimitriy'

from utils.decorators import ignore_exception
from utils.datetime_helper import date_parse


def bool_parse(v):
    return v.lower() in ("true", "on", "1", "yes", "t")


get_safe_int = ignore_exception(ValueError)(int)

get_safe_date = ignore_exception(ValueError)(date_parse)

get_safe_bool = ignore_exception(ValueError)(bool_parse)