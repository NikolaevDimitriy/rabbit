__author__ = 'dimitriy'

import settings as ls
from aiopg.sa.engine import create_engine
from aioredis import create_pool
from sqlalchemy.dialects.postgresql.psycopg2 import PGDialect_psycopg2
from utils.json_helper import serialize, deserialize
from psycopg2 import extras
import aiomysql


_dialect = PGDialect_psycopg2(json_serializer=serialize)
_dialect.implicit_returning = True
_dialect.supports_native_enum = True
_dialect.supports_smallserial = True  # 9.2+
_dialect._backslash_escapes = False
_dialect.supports_sane_multi_rowcount = True  # psycopg 2.0.9+
_dialect._has_native_hstore = True


class PgProvider(object):
    def __init__(self, pg_master, pg_slave):
        self.pg_master = pg_master
        self.pg_slave = pg_slave

    def close(self):
        self.pg_master.close()
        self.pg_slave.close()

    async def wait_closed(self):
        await self.pg_master.wait_closed()
        await self.pg_slave.wait_closed()


class DataProviders(object):

    def __init__(self, pg_db=None, redis=None, mongo=None, search_engine=None):
        self.pg_db = pg_db
        self.redis = redis
        self.mongo = mongo
        self.search_engine = search_engine

    def close(self):
        self.pg_db.close()
        self.redis.close()
        self.search_engine.close()

    async def wait_closed(self):
        await self.pg_db.wait_closed()
        await self.redis.wait_closed()
        await self.search_engine.wait_closed()


async def create_data_providers(loop) -> DataProviders:
    return DataProviders(pg_db=await create_pgdb_engine(loop), redis=await create_redis_engine(loop),
                         mongo=None, search_engine=await create_serche_engine(loop))


async def create_pgdb_engine(loop):
    pg_master = await create_engine(user=ls.BD_MASTER_USER,
                                    database=ls.DB_MASTER_NAME,
                                    host=ls.DB_MASTER_HOST,
                                    password=ls.BD_MASTER_PWD,
                                    dialect=_dialect,
                                    loop=loop)
    pg_slave = await create_engine(user=ls.BD_SLAVE_USER,
                                   database=ls.DB_SLAVE_NAME,
                                   host=ls.DB_SLAVE_HOST,
                                   password=ls.BD_SLAVE_PWD,
                                   dialect=_dialect,
                                   loop=loop)

    extras.register_default_jsonb(globally=True, loads=deserialize)
    return PgProvider(pg_master, pg_slave)


async def create_redis_engine(loop):
    redis_pool = await create_pool((ls.SESSION_REDIS_HOST, ls.SESSION_REDIS_PORT), db=ls.SESSION_REDIS_DB,
                                   encoding=ls.SESSION_REDIS_JSON_ENCODING, loop=loop)
    return redis_pool

async def create_serche_engine(loop):
    search_engine = await aiomysql.create_pool(host=ls.SEARCH_ENGINE_HOST,
                                               port=ls.SEARCH_ENGINE_PORT,
                                               loop=loop,
                                               user='root', password='',
                                               charset='utf8', use_unicode=True)
    return search_engine
