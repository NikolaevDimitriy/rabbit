__author__ = 'dimitriy'
import sqlalchemy as sa
from storages.base import PgBaseStorage
from collections import namedtuple
from typing import List
from sqlalchemy.sql.expression import literal_column, and_


class Condition(namedtuple('Condition', ['identity', 'value'])):
    pass


class PgUniqueChecker(PgBaseStorage):

    async def check_exists(self, conditions: List[Condition]):
        query = sa.select([literal_column(self.get_field_column(conditions[0].identity)),
                           sa.exists().where(self.and_conditions(conditions[0].value))])
        if len(conditions) > 1:
            others = []
            for condition in conditions[1:]:
                others.append(sa.select([literal_column(self.get_field_column(condition.identity)),
                                         sa.exists().where(self.and_conditions(condition.value))]))
            query = query.union(*others)
        res = await self.pg_fetch(query)
        return res

    def get_field_column(self, value):
        return "'{0}' as field".format(value)

    def and_conditions(self, condition):
        if isinstance(condition, list):
            return and_(*condition)
        else:
            return condition

    async def pg_fetch(self, query):
        async with self.pg_db.pg_master.acquire() as conn:
            res = {}
            async for row in conn.execute(query):
                res[row[0]] = row[1]
            return res