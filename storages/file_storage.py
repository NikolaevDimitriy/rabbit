__author__ = 'dimitriy'
import aiofiles
import os
import asyncssh
import abc


class FileStorageBase:
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    async def save(self, path, filename, data):
        pass

    @abc.abstractmethod
    async def get_file(self, filename):
        pass


class LocalFileSystemStorage(FileStorageBase):

    def __init__(self, loop=None, **kwargs):
        self.loop = loop

    async def save(self, path, filename, data):
        if not os.path.exists(path):
            os.makedirs(path)
        fullname = os.path.join(path, filename)
        async with aiofiles.open(fullname, mode='wb', loop=self.loop) as f:
            await f.write(data)

    async def get_file(self, filename):
        if os.path.isfile(filename):
            async with aiofiles.open(filename, mode='rb', loop=self.loop) as f:
                content = await f.read()
                return content
        return None


class SftpFileStorage(FileStorageBase):

    def __init__(self, loop=None, **kwargs):
        self.loop = loop
        self.host = kwargs.get('host', None)
        self.port = kwargs.get('port', 22)
        self.user = kwargs.get('user', '')
        self.pwd = kwargs.get('pwd', '')

    async def save(self, path, filename, data):
        async with asyncssh.connect(host=self.host, port=self.port,
                                    username=self.user, password=self.pwd,
                                    loop=self.loop) as conn:
            async with conn.start_sftp_client() as sftp:
                is_dir_exists = await sftp.exists(path)
                if not is_dir_exists:
                    await sftp.mkdir(path)
                fullname = os.path.join(path, filename)
                async with sftp.open(fullname, pflags_or_mode='w', encoding=None) as f:
                    await f.write(data)

    async def get_file(self, filename):
        async with asyncssh.connect(host=self.host, port=self.port,
                                    username=self.user, password=self.pwd,
                                    loop=self.loop) as conn:
            async with conn.start_sftp_client() as sftp:
                is_exists = await sftp.isfile(filename)
                if is_exists:
                    async with sftp.open(filename, pflags_or_mode='r', encoding=None) as f:
                        context = await f.read()
                        return context
        return None