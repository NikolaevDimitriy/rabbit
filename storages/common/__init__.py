__author__ = 'dimitriy'

from models.common.address import Address
from models.common.attachment import Attachment
from models.common.geo import Geo, Country
from storages.base import PgBaseStorage


class AddressStorage(PgBaseStorage):
    model = Address


class CountryStorage(PgBaseStorage):
    model = Country


class GeoStorage(PgBaseStorage):
    model = Geo


class AttachmentStorage(PgBaseStorage):
    model = Attachment

