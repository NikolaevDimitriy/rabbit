__author__ = 'dimitriy'

import abc
import ujson as json
from utils.json_helper import serialize_entity
from storages import DataProviders, PgProvider
from sqlalchemy.sql.expression import and_
from sqlalchemy.sql.expression import bindparam


class BaseStorage(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    async def save(self, instance, fields=(), insert_if_not_new=False, **kwargs):
        return False

    @abc.abstractmethod
    async def delete(self, instance):
        return False

    @abc.abstractmethod
    async def get(self, pk):
        return None

    @abc.abstractmethod
    async def get_list(self, **kwargs):
        return []


class PgBaseStorage(BaseStorage):
    model = None

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.pg_db = data_providers.pg_db

    async def save(self, instance, fields=(), insert_if_not_new=False, **kwargs):
        arg = self.get_args_add_or_update(instance, fields)
        try:
            if instance.is_new:
                instance.id = await self.pg_scalar(self.get_query_insert(**arg))
            elif insert_if_not_new:
                await self.pg_execute(self.get_query_insert(**arg))  # pregenerated id
            else:
                query = self.get_query_update(instance.id, **arg)
                if query is not None:
                    await self.pg_execute(query)
                else:
                    return False
            return True
        except Exception as x:
            print(x)
            return False

    async def bulk_save(self, instances, fields=(), **kwargs):
        try:
            flds = self.get_args_bulk_add_or_update(fields)
            ins_values = [{f: getattr(inst, f) for f in flds} for inst in instances if inst.is_new]
            if len(ins_values) > 0:
                insert_stmt = self.get_bulk_query_insert(flds)
                res = await self.pg_execute(insert_stmt, ins_values)
            if len(ins_values) != len(instances):
                update_stmt = self.get_bulk_query_update(flds)
                flds.append(self.model.tbc.id.name)
                upd_values = [{f: getattr(inst, f) for f in flds} for inst in instances if not inst.is_new]
                await self.pg_execute(update_stmt, upd_values)
            return True
        except Exception as x:
            print(x)
            return False

    async def delete(self, instance):
        try:
            query = self.get_query_delete(instance.id)
            if query is not None:
                await self.pg_execute(query)
                return True
        except:
            return False

    async def get(self, pk):
        try:
            query = self.get_query_single(pk)
            if query is not None:
                res = await self.pg_first(query)
                return res
        except:
            pass
        return None

    async def get_list(self, **kwargs):
        try:
            query = self.get_query_list(**kwargs)
            if query is not None:
                res = await self.pg_fetch(query)
                return res
        except Exception as x:
            print(x)
        return []

    def get_query_single(self, pk):
        if self.model and pk is not None and pk != 0:
            return self.model.tb.select().where(self.model.tbc.id == pk)

    def get_query_update(self, pk, **kwargs):
        if self.model and pk is not None and pk != 0:
            return self.model.tb.update().values(**kwargs).where(self.model.tbc.id == pk)

    def get_bulk_query_update(self, fields):
        if self.model:
            return self.model.tb.update().where(self.model.tbc.id == bindparam(self.model.tbc.id.name)).values(
                {field: bindparam(field) for field in fields})

    def get_bulk_query_insert(self, fields):
        if self.model:
            return self.model.tb.insert().values({field: bindparam(field) for field in fields})

    def get_query_insert(self, **kwargs):
        if self.model:
            return self.model.tb.insert().values(**kwargs)

    def get_query_delete(self, pk):
        if self.model and pk is not None and pk != 0:
            return self.model.tb.delete().where(self.model.tbc.id == pk)

    def get_query_list(self, **kwargs):
        if self.model:
            query = self.model.tb.select()
            if kwargs:
                if 'order_by' in kwargs and kwargs['order_by']:
                    query = query.order_by(kwargs['order_by'])
                conditions = kwargs.get('conditions', [])
                if conditions:
                    query = query.where(and_(*conditions))
                limit = kwargs.get('limit', 0)
                page = kwargs.get('page', 0)
                if limit is not None and limit > 0 and page is not None and page > 0:
                    query = query.limit(limit).offset(page*limit)
            return query

    def get_args_add_or_update(self, instance, fields=()):
        args = {}
        for col in self.model.tbc if not fields else fields:
            col_name = col if isinstance(col, str) else col.name
            if ((not isinstance(col, str) and not col.primary_key) or isinstance(col, str)) and hasattr(instance, col_name):
                args[col_name] = getattr(instance, col_name)
        return args

    def get_args_bulk_add_or_update(self, fields=()):
        args = []
        for col in self.model.tbc if not fields else fields:
            col_name = col if isinstance(col, str) else col.name
            if (not isinstance(col, str) and not col.primary_key) or isinstance(col, str):
                args.append(col_name)
        return args

    async def pg_execute(self, query, *multiparams):
        async with self.pg_db.pg_master.acquire() as conn:
            res = await conn.execute(query, *multiparams)
            return res

    async def pg_first(self, query):
        async with self.pg_db.pg_slave.acquire() as conn:
            res = await conn.execute(query)
            first = await res.first()
            if first is not None:
                return self.model(**dict(first))
            return first

    async def pg_fetch(self, query):
        async with self.pg_db.pg_slave.acquire() as conn:
            res = []
            async for row in conn.execute(query):
                res.append(self.model(**dict(row)))
            return res

    async def pg_scalar(self, query):
        async with self.pg_db.pg_master.acquire() as conn:
            res = await conn.scalar(query)
            return res


class RedisBaseStorageMixin:

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.redis = data_providers.redis

    async def save_to_redis(self, instance, expire=0):
        key = instance.get_redis_key()
        if key:
            try:
                data = serialize_entity(instance)
                async with self.redis.get() as r:
                    await r.set(key, data, expire=expire)
                    return True
            except:
                return False
        return False

    async def get_from_redis(self, key):
        if key:
            try:
                async with self.redis.get() as r:
                    raw = await r.get(key)
                    if raw is not None:
                        res = self.model().from_dict(**json.loads(raw))
                        return res
            except:
                return None
        else:
            return None

    async def delete_from_redis(self, key):
        if key:
            try:
                async with self.redis.get() as r:
                    await r.delete(key)
                    return True
            except:
                return False
        else:
            return False

    async def bulk_save_to_redis(self, instances, expire=0):
        pass



# wait motor 1.0 release
class MongoBaseStorageMixin:
    def __init__(self, data_providers: DataProviders, **kwargs):
        self.mongo = data_providers.mongo

    async def save_to_mongo(self, instance):
        try:
            data = serialize_entity(instance)
            return True
        except:
            return False

    async def get_from_mongo(self, key):
        try:
            return None
        except:
            return None

    async def delete_from_mongo(self, key):
        try:
            return True
        except:
            return False


class PgRedisBaseStorage(RedisBaseStorageMixin, PgBaseStorage):
    save_to_redis_if_not_exist = True

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.pg_db = data_providers.pg_db
        self.redis = data_providers.redis

    async def save(self, instance, fields=(), insert_if_not_new=False, expire=0):
        res = await super(PgRedisBaseStorage, self).save(instance, fields, insert_if_not_new)
        if res:
            res = await self.save_to_redis(instance, expire)
        return res

    async def get(self, pk):
        if pk is not None and pk != 0:
            instance = self.model(id=pk)
            res = await self.get_from_redis(instance.get_redis_key())
            if res is not None:
                return res
            res = await super(PgRedisBaseStorage, self).get(pk)
            if self.save_to_redis_if_not_exist and res is not None:
                await self.save_to_redis(res)
            return res
        return None

    async def delete(self, instance):
        res = await super(PgRedisBaseStorage, self).delete(instance)
        if res:
            res = await super(PgRedisBaseStorage, self).delete_from_redis(instance.get_redis_key())
        return res



