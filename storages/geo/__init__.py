__author__ = 'dimitriy'

from models.common.geo import Geo, Country
from storages.base import PgBaseStorage
from storages.search_base import SphinxSearchBase
import uuid


class CountryStorage(PgBaseStorage):
    model = Country


class GeoStorage(PgBaseStorage):
    model = Geo


class GeoSearchStorage(SphinxSearchBase):
    index = 'cities'
    ordering_by = 'address_level'
    ordering_dir = "ASC"

    def get_query(self, **kwargs):
        query = super(GeoSearchStorage, self).get_query(**kwargs)
        conditions = kwargs.get('conditions', None)
        if query is not None:
            if conditions is not None:
                for key, val in conditions.items():
                    query += " and %(key)s = %(val)s" % {'key': key, 'val': val, }
            query += self.get_ordering()
        return query

    def convert(self, row):
        geo = Geo()
        geo.id = row[0]
        geo.name = row[1]
        geo.full_name = row[2]
        geo.parent_id = row[3]
        geo.country_id = row[4]
        geo.address_level = row[5]
        geo.address_type = row[6]
        geo.is_fias = bool(row[7])
        geo.global_fias_id = uuid.UUID(row[8]) if row[8] != '' else None
        geo.parent_fias_id = uuid.UUID(row[9]) if row[9] != '' else None
        geo.official_name = row[10]
        geo.name_en = row[11]
        return geo
