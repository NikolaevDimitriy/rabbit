from models.account.user import User
from storages.utility_storage import PgUniqueChecker
from storages.account import UserStorage
from storages.base import PgBaseStorage
from utils.types import KeyValue
from utils.json_helper import serialize_entity

__author__ = 'dimitriy'


class UserUniqueChecker(PgUniqueChecker):
    model = User


# class BulkUserSaver(UserStorage):
#
#     async def save(self, instance, fields=(), insert_if_not_new=False, expire=0):
#         pg_res = []
#         for user in instance:
#             res = await super(PgBaseStorage, self).save(user, fields, insert_if_not_new)
#             pg_res.append(res)
#         redis_res = self.bulk_save_to_redis(instance)
#         return zip(pg_res, redis_res)
