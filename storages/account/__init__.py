__author__ = 'dimitriy'
from models.account.user import User
from storages.base import PgRedisBaseStorage


class UserStorage(PgRedisBaseStorage):
    model = User

    async def get_single_from_sql(self, condition):
        query = self.model.tb.select(self.model.tb).where(condition)
        user = await self.pg_first(query)
        return user


