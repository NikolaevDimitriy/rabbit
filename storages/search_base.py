__author__ = 'dimitriy'
from storages import DataProviders


class SphinxSearchBase(object):
    index = None
    ordering_by = None
    ordering_dir = None

    def __init__(self, data_providers: DataProviders, **kwargs):
        self.search_engine = data_providers.search_engine

    def get_query(self, **kwargs):
        query = kwargs.get('query', None)
        if self.index is not None and query is not None:
            return "SELECT * FROM %(index)s WHERE MATCH('%(query)s*')" % {'index': self.index, 'query': query, }
        return None

    def get_ordering(self):
        if self.ordering_by and self.ordering_dir:
            return " ORDER BY %s %s" % (self.ordering_by, self.ordering_dir, )
        return ""

    def convert(self, row):
        return row

    async def get_list(self, **kwargs):
        res = []
        async with self.search_engine.get() as conn:
            async with conn.cursor() as cur:
                await cur.execute(self.get_query(**kwargs)+";")
                rows = await cur.fetchall()
                for row in rows:
                    res.append(self.convert(row))
        return res

