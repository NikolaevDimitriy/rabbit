__author__ = 'dimitriy'

from models.customer.customer import Customer
from storages import DataProviders
from storages.base import PgRedisBaseStorage
from storages.common import AddressStorage
from storages.utility_storage import PgUniqueChecker


class CustomerStorage(PgRedisBaseStorage):
    model = Customer
    insert_fields = ()

    def __init__(self, data_providers: DataProviders, **kwargs):
        super(CustomerStorage, self).__init__(data_providers, **kwargs)
        self._address_storage = AddressStorage(data_providers)

    async def save(self, instance, fields=(), insert_if_not_new=False, expire=0):
        fields_str = [col if isinstance(col, str) else col.name for col in fields]
        if not fields or Customer.actual_address_id.name in fields_str:
            res = await self._address_storage.save(instance.actual_address)
            if res:
                instance.actual_address_id = instance.actual_address.id
            else:
                return False
        if instance.actual_address.is_equal_to(instance.legal_address):
            instance.legal_address.id = instance.actual_address.id
            instance.legal_address_id = instance.actual_address.id

        elif not fields or Customer.legal_address_id.name in fields_str:
            res = await self._address_storage.save(instance.legal_address)
            if res:
                instance.legal_address_id = instance.legal_address.id
            else:
                return False

        res = await super(CustomerStorage, self).save(instance, fields, insert_if_not_new, expire)
        return res


class CustomerUniqueChecker(PgUniqueChecker):
    model = Customer
