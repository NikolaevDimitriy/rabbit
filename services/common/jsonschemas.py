__author__ = 'dimitriy'

address_schema = {
    '$schema': 'http://json-schema.org/draft-04/schema#',
    'description': 'An address',
    'type': 'object',
    'properties': {
        'country_id': {'type': 'integer'},
        'country_name': {'type': 'string'},
        'region_id': {'type': 'integer'},
        'region_name': {'type': 'string'},
        'district_id': {'type': 'integer'},
        'district_name': {'type': 'string'},
        'city_id': {'type': 'integer'},
        'city_name': {'type': 'string'},
        'street': {'type': 'string'},
        'building': {'type': 'string'},
        'apartment': {'type': 'string'},
        'underground_station_id': {'type': 'integer'},
        'underground_station_name': {'type': 'string'},
        'latitude': {'type': 'number'},
        'longitude': {'type': 'number'}
    },
    'required': ['city_id', 'city_name', 'street', 'building', 'apartment'],
    'additionalProperties': False,
}
