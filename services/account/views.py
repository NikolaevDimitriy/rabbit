__author__ = 'dimitriy'

from web_core.views import SingleJsonObjectEditView
from web_core.security.decorators import auth_required_view
from models.account.user import User
from ..account.controllers import MyAccountController
from storages.account import UserStorage


@auth_required_view
class MyAccountView(SingleJsonObjectEditView):
    model = User
    controller = MyAccountController
    storage = UserStorage

    def get_pk(self):
        return self.request.user.id

    def convert_context_to_json(self, context, **kwargs):
        arg = {'minimal': True, 'include_pwd': False}
        return super(MyAccountView, self).convert_context_to_json(context, **arg)
