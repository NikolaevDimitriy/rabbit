__author__ = 'dimitriy'

from web_core.application import AIOApplication
from services.account.views import MyAccountView


class AioAccountApplication(AIOApplication):

    def name(self):
        return 'account'

    def get_aio_urls(self):
        url_patterns = (
            ('my_account', MyAccountView, 'my_account'),
        )
        return url_patterns

aio_application = AioAccountApplication()