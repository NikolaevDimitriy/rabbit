__author__ = 'dimitriy'
from web_core.validators import ClientDataValidationBase
from jsonschema import Draft4Validator, FormatChecker
from ..account.jsonschemas import user_schema


class MyAccountJsonValidator(ClientDataValidationBase):
    validator = Draft4Validator(user_schema, format_checker=FormatChecker(('email',)))

