__author__ = 'dimitriy'
from web_core.controllers import BaseAddOrUpdateController
from utils.types.return_types import SIEResult
from ..account.validators import MyAccountJsonValidator
from business.account import MyAccount
from models.account.user import User


class MyAccountController(BaseAddOrUpdateController):
    validator = MyAccountJsonValidator
    model = User

    async def update(self) -> SIEResult:
        res = await super(MyAccountController, self).update()
        if res.success:
            bl = MyAccount(self._user, res.instance, self._data_providers)
            res = await bl.save()
            return res
        return res

    def populate_obj(self):
        obj = super(MyAccountController, self).populate_obj()
        obj.id = self._user.id
        return obj
