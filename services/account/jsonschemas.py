__author__ = 'dimitriy'

import pytz

user_schema = {
    'type': 'object',
    'properties': {
        'email': {'type': 'string'},
        'phone': {'type': 'string'},
        'first_name': {'type': 'string',  "minLength": 2},
        'last_name': {'type': 'string',  "minLength": 2},
        'timezone': {'enum': [tz for tz in pytz.country_timezones('RU')]},
        'role': {'type': 'integer'}
    },
    "additionalProperties": False,
    'anyOf': [
        {'properties': {
            'email': {
                'format': 'email',
            }
        }},
        {'properties': {
            'phone': {
                'pattern': '^((8|\+7)[-\s]?)?(\(?\d{3}\)?[-\s]?)?[-\d\s]{7,10}$',
            }
        }},
    ],
    'required': ['first_name', 'last_name', 'timezone', 'role']
}
