from web_core.validators import ClientDataValidationBase
from jsonschema import Draft4Validator, FormatChecker
from ..customer.jsonschemas import individual_customer_schema, legal_entity_customer_schema


class IndividualValidator(ClientDataValidationBase):
    validator = Draft4Validator(individual_customer_schema, format_checker=FormatChecker(('date',)))


class LegalEntityValidator(ClientDataValidationBase):
    validator = Draft4Validator(legal_entity_customer_schema, format_checker=FormatChecker(('date',)))
