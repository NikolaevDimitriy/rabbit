from utils.types.return_types import SIEResult
from web_core.controllers import BaseAddOrUpdateController
from ..customer.validators import IndividualValidator, LegalEntityValidator
from business.customer import MyCustomer
from models.common.address import Address
from models.customer.customer import Customer, IndividualRequisitesNames as _inames


class CustomerControllerBase(BaseAddOrUpdateController):
    model = Customer
    async def update(self) -> SIEResult:
        res = await  super(CustomerControllerBase, self).update()
        if res.success:
            bl = MyCustomer(self._user, res.instance, self._data_providers)
            res = await bl.save()
            return res
        return res

    def populate_obj(self):
        obj = self.get_model().get_default(user=self._user)
        obj.type = self._data['type']
        obj.requisites = self._data['requisites']
        obj.phone = self._data['phone']

        if obj.is_individual():
            patronymic = obj.requisites.get(_inames.patronymic, None)
            if patronymic is not None and patronymic != '':
                obj.name = '{0} {1} {2}'.format(obj.requisites.get(_inames.first_name, ''), patronymic,
                                                obj.requisites.get(_inames.last_name, ''))
            else:
                obj.name = '{0} {1}'.format(obj.requisites.get(_inames.first_name, ''),
                                            obj.requisites.get(_inames.last_name, ''))
        else:
            obj.name = self._data['name']

        obj.status = 0
        actual_address = Address(**self._data['actual_address'])
        actual_address.deleted = False
        legal_address = Address(**self._data['legal_address'])
        actual_address.deleted = False
        obj.actual_address_prop = actual_address
        obj.legal_address_prop = legal_address
        obj.id = self._user.customer.id if self._user.customer else None
        return obj


class IndividualController(CustomerControllerBase):
    validator = IndividualValidator


class LegalEntityController(CustomerControllerBase):
    validator = LegalEntityValidator

