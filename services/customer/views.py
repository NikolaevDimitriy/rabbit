__author__ = 'amicus'

from web_core.views import JsonDetailView, SingleJsonObjectEditView
from web_core.security.decorators import auth_required_view
from models.customer.customer import Customer
from storages.customer import CustomerStorage
from ..customer.controllers import IndividualController, LegalEntityController


class MyCustomerView(JsonDetailView):
    model = Customer
    storage = CustomerStorage
    provide_default = True

    def get_pk(self):
        return self.request.user.customer.id if self.request.user.customer else 0


@auth_required_view
class LegalEntityView(SingleJsonObjectEditView):
    model = Customer
    controller = LegalEntityController
    storage = CustomerStorage


@auth_required_view
class IndividualView(SingleJsonObjectEditView):
    Model = Customer
    controller = IndividualController
    storage = CustomerStorage
