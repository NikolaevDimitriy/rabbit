__author__ = 'amicus'

from web_core.application import AIOApplication
from services.customer.views import MyCustomerView, IndividualView, LegalEntityView


class AioCustomerApplication(AIOApplication):

    def name(self):
        return 'customer'

    def get_aio_urls(self):
        url_patterns = (
            ('my_customer', MyCustomerView, 'my_customer'),
            ('my_individual_customer', IndividualView, 'individual'),
            ('my_legal_entity_customer', LegalEntityView, 'legal_entity'),
        )
        return url_patterns

aio_application = AioCustomerApplication()
