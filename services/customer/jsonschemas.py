from ..common.jsonschemas import address_schema
from models.customer.customer import IndividualRequisitesNames as _inames, LegalEntityRequisitesNames as _lenames
from models.customer import CUSTOMER_TYPES_OF_OWNERSHIP
import copy
from utils.datetime_helper import date_ru_pattern
_individual_schema = {
    'type': 'object',
    'properties': {
        _inames.passport_id: {'type': 'string'},
        _inames.issue_date: {'pattern': date_ru_pattern},
        _inames.first_name: {'type': 'string'},
        _inames.patronymic: {'type': 'string'},
        _inames.last_name: {'type': 'string'},
        _inames.birth_date: {'pattern': date_ru_pattern},
        _inames.sex: {'type': 'boolean'},
    },
    'required': [_inames.passport_id, _inames.issue_date, _inames.first_name, _inames.patronymic, _inames.last_name,
                 _inames.birth_date, _inames.sex],
    'additionalProperties': False
}

_legal_entity_schema = {
    'type': 'object',
    'properties': {
        _lenames.type_of_ownership: {'enum': [x.key for x in CUSTOMER_TYPES_OF_OWNERSHIP]},
        _lenames.register_date: {'pattern': date_ru_pattern},
        _lenames.bank_name: {'type': 'string'},
        _lenames.current_account: {'pattern': '^\d{20}$'},
        _lenames.correspondent_account: {'pattern': '^\d{20}$'},
        _lenames.inn: {'pattern': '^\d{12}$'},
        _lenames.kpp: {'pattern': '^\d{9}$'},
        _lenames.okpo: {'pattern': '^\d{10}$'},
        _lenames.oktmo: {'pattern': '^\d{8}\s?\d{11}$'},
        _lenames.ogrn: {'pattern': '^\d{13}$'},
        _lenames.bik: {'pattern': '^\d{12}$'},
    },
    'required': [_lenames.type_of_ownership, _lenames.register_date, _lenames.inn],
    'additionalProperties': False
}

_customer_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'legal_address': {'$ref': '#/definitions/address'},
        'actual_address': {'$ref': '#/definitions/address'},
        'phone': {'type': 'string'},
        'requisites': {'$ref': '#/definitions/requisites'}
    },
    'additionalProperties': False,
    'required': ['legal_address', 'actual_address', 'phone', 'requisites'],
    'definitions': {
        'address': address_schema
    }
}

individual_customer_schema = copy.deepcopy(_customer_schema)
individual_customer_schema['properties']['type'] = {'enum': [2]}  # add individual type prop
individual_customer_schema['definitions']['requisites'] = _individual_schema

legal_entity_customer_schema = copy.deepcopy(_customer_schema)
legal_entity_customer_schema['properties']['type'] = {'enum': [1]}  # add legal_entity type prop
legal_entity_customer_schema['definitions']['requisites'] = _legal_entity_schema
legal_entity_customer_schema['required'].append('name')
