__author__ = 'dimitriy'

from enum import IntEnum


class Permissions(IntEnum):
    access_denied = 0,
    readonly = 1,
    full_access = 2,
    optional = 4
