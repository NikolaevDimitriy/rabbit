__author__ = 'dimitriy'
import functools
import inspect
from aiohttp import web
from aiohttp import hdrs


def auth_required(f):
    @functools.wraps(f)
    async def wrapped(self, *args, **kwargs):
        if not self.is_authenticate():
            # raise web.HTTPForbidden()
            return web.HTTPFound('/login')
        return await f(self, *args, **kwargs)
    return wrapped


def auth_required_view(cls):
    def wrapper(type):
        for name, func in inspect.getmembers(type, predicate=inspect.isfunction):
            if name.upper() in hdrs.METH_ALL:
                setattr(type, name, auth_required(func))
        return type
    return wrapper(cls)

