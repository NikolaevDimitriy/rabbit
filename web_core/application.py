__author__ = 'dimitriy'
import abc
from urllib.parse import urljoin


class AIOApplication(object):

    methods = ('GET', 'POST')

    __metaclass__ = abc.ABCMeta

    @abc.abstractproperty
    def name(self):
        pass

    def get_aio_urls(self):
        return ()

    @property
    def aio_urls(self):
        return self.get_aio_urls()

    @staticmethod
    def include(prefix, url_list, name=''):
        flat_list = []
        for url in url_list:
            if isinstance(url, list):
                flat = AIOApplication.include(prefix, url)
                for u in flat:
                    flat_list.append((u[0], u[1], '%s%s' % (name+':' if name else '', u[2],)))
            else:
                combine_url = urljoin(prefix, url[0]) if url[0] != '' else prefix[:-1] if prefix != '/' else prefix
                combine_name = '%s%s' % (name+':' if name else '', url[2],)
                flat_list.append((combine_url, url[1], combine_name))

        return flat_list

    def get_nav_items(self):
        return ()

    @property
    def menu(self):
        return self.get_nav_items()

    @staticmethod
    def include_nav(nav_list):
        flat_list = []
        for node in nav_list:
            if isinstance(node, list):
                flat = AIOApplication.include(node)
                for n in flat:
                    flat_list.append(n)
            else:
                flat_list.append(node)
        return flat_list
