__author__ = 'dimitriy'
import asyncio
from wtforms import Form
from wtforms.compat import iteritems
from wtforms.csrf.session import SessionCSRF
from datetime import timedelta


class BaseForm(Form):
    def __init__(self, app, formdata=None, obj=None, prefix='', data=None, meta=None, **kwargs):
        super(BaseForm, self).__init__(formdata=formdata,
                                       obj=obj,
                                       prefix=prefix,
                                       data=data, meta=meta,
                                       **kwargs)
        self._app = app
        self.out_errors = None

    @property
    def app(self):
        return self._app

    class Meta:
        csrf = True
        csrf_class = SessionCSRF
        csrf_secret = b'EPj00jpfj8Gx1SjnyLxwBBSQfnQ9DJYe0Ym'
        csrf_time_limit = timedelta(minutes=20)

    async def validate(self):
        """
        Validates the form by calling `validate` on each field, passing any
        extra `Form.validate_<fieldname>` validators to the field validator.
        """
        extra_validators = {}
        for name in self._fields:
            inline = getattr(self.__class__, 'validate_%s' % name, None)
            if inline is not None:
                extra_validators[name] = [inline]

        self._errors = None
        success = True
        for name, field in iteritems(self._fields):
            if extra_validators is not None and name in extra_validators:
                extra = extra_validators[name]
            else:
                extra = tuple()
            is_field_valid = await field.validate(self, extra) if asyncio.iscoroutinefunction(field.validate) else field.validate(self, extra)
            if not is_field_valid:
                success = False
        return success

