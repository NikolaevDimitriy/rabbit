__author__ = 'dimitriy'
from utils.exceptions import ImproperlyConfigured


class ControllerValidationMixin(object):
    validator = None

    def validate(self, data):
        v_instance = self.get_validator()(data)
        return v_instance.validate()

    def get_validator(self):
        if self.validator:
            return self.validator
        raise ImproperlyConfigured("ControllerValidationMixin requires either a definition of 'validator'")


