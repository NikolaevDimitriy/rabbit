__author__ = 'dimitriy'
import abc

from utils.exceptions import ImproperlyConfigured
from utils.types.return_types import SIEResult
from web_core.controllers.mixins import ControllerValidationMixin


class BaseController(metaclass=abc.ABCMeta):

    def __init__(self, user=None, data_providers=None, **kwargs):
        self._user = user
        self._data = kwargs.get('data', None)
        self._data_providers = data_providers


class BaseAddOrUpdateController(ControllerValidationMixin, BaseController):
    model = None

    def get_model(self):
        if self.model is not None:
            return self.model
        raise ImproperlyConfigured("CRUDController requires either a definition of "
                                   "'model' or an implementation of 'get_model' method")

    @abc.abstractmethod
    async def update(self) -> SIEResult:
        errors = self.validate(self._data)
        if errors:
            return SIEResult(False, None, errors)
        return SIEResult(True, self.populate_obj(), None)

    def populate_obj(self):
        obj = self.get_model()(**self._data)  # self._data: dict
        return obj


class BaseCRUDController(BaseAddOrUpdateController):

    @abc.abstractmethod
    async def create(self) -> SIEResult:
        pass

    @abc.abstractmethod
    async def read(self) -> SIEResult:
        pass

    async def update(self) -> SIEResult:
        return SIEResult(True, None, None)

    @abc.abstractmethod
    async def delete(self) -> SIEResult:
        pass







