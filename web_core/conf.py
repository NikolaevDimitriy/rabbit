import os
import sys

from web_core import defaults


__all__ = ['settings']


def _load_settings():
    module_name = os.environ.get("AIOWEB_SETTINGS_MODULE", 'settings')
    cwd = os.getcwd()
    if cwd not in sys.path:
        sys.path.append(cwd)
    module = __import__(module_name, fromlist='*')
    for attr in dir(defaults):
        if not hasattr(module, attr):
            setattr(module, attr, getattr(defaults, attr))
    return module

settings = _load_settings()
