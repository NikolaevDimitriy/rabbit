__author__ = 'dimitriy'

from typing import List
from utils.types.return_types import ErrorResult
from utils.localization import gettext_local as _
from utils.exceptions import ImproperlyConfigured


class Draft4ErrorAdapter(object):

    def __init__(self, validators=None):
        self.validators = validators

    def adapt_errors(self, errors) -> List[ErrorResult]:
        res = []
        for x in errors:
            adapter = self.validators.get(x.validator, None)
            if adapter:
                res.extend(adapter(self.validators).adapt_error(x))
            else:
                raise ImproperlyConfigured("Map of validators does not contain validator '{0}'".format(x.validator))
        return res


class ErrorAdapterBase(Draft4ErrorAdapter):
    message = 'Error'

    def adapt_error(self, error) -> List[ErrorResult]:
        return [ErrorResult(identity='/'.join(error.absolute_path), errors=[str(self.message)])]


class EnumErrorAdapter(ErrorAdapterBase):
    message = _('Value is not a part of given set.')


class MinLengthErrorAdapter(ErrorAdapterBase):
    message = _('Value is to short.')


class FormatErrorAdapter(ErrorAdapterBase):
    message = _('Value does not match to validation pattern.')


class TypeErrorAdapter(ErrorAdapterBase):
    message = _('Invalid type.')


class AdditionalPropsErrorAdapter(ErrorAdapterBase):
    message = _('Additional data are not allowed.')

    def adapt_error(self, error) -> List[ErrorResult]:
        identity = error.path[0] if error.path else '_all_'
        return [ErrorResult(identity=identity, errors=[str(self.message)])]


class RequiredErrorAdapter(ErrorAdapterBase):
    message = _('Missing required data.')

    def adapt_error(self, error) -> List[ErrorResult]:
        identity = error.path[0] if error.path else '_all_'
        return [ErrorResult(identity=identity, errors=[str(self.message)])]


class AnyOfErrorAdapter(ErrorAdapterBase):
    message = _('Any of fields are required (%s).')

    def adapt_error(self, error) -> List[ErrorResult]:
        identity = error.path[0] if error.path else '_all_'
        fields = {x.path[0] for x in error.context}
        anyof_error = ErrorResult(identity=identity, errors=[str(self.message) % ' or '.join(fields)])
        relative_errors = self.adapt_errors(error.context)
        relative_errors.append(anyof_error)
        return relative_errors


map_of_validators = {
    'enum': EnumErrorAdapter,
    'type': TypeErrorAdapter,
    'anyOf': AnyOfErrorAdapter,
    'format': FormatErrorAdapter,
    'pattern': FormatErrorAdapter,
    'required': RequiredErrorAdapter,
    'minLength': MinLengthErrorAdapter,
    'additionalProperties': AdditionalPropsErrorAdapter
}
