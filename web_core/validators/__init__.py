__author__ = 'dimitriy'
from utils.localization import gettext_local as _
from web_core.validators.json_schema import Draft4ErrorAdapter, map_of_validators


class ClientDataValidationBase(object):
    validator = None
    error_adapter = Draft4ErrorAdapter(map_of_validators)

    def __init__(self, data=None):
        self._data = data
        self._errors = []

    def validate(self):
        if self._data is None:
            self._errors.append(_("Empty data"))
        if self.validator:
            self._errors.extend(self.error_adapter.adapt_errors(self.validator.iter_errors(self._data)))
        return self._errors
