__author__ = 'dimitriy'

from web_core.middlewares.csrf import CSRF_TOKEN_NAME

async def csrf_processor(request):
    return {'csrf_token': request[CSRF_TOKEN_NAME]}
