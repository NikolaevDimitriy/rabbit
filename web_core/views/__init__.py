__author__ = 'dimitriy'
from aiohttp.web import View
from aiohttp_session import SESSION_KEY

from utils import json_helper as jhelp
from web_core.views import mixins as mx


class BaseView(mx.ApplicationBasePageMixin, View):
    def is_authenticate(self):
        return hasattr(self.request, 'user') and self.request.user is not None

    def get_url_param(self, name, default):
        return self._request.match_info.get(name, default)

    def get_query_param(self, name, default):
        return self._request.GET.get(name, default)

    def get_post_param(self, name, default):
        return self._request.POST.get(name, default)


class TemplateView(mx.TemplateResponseMixin, mx.ContextMixin, BaseView):
    async def get(self, *args, **kwargs):
        context = await self.get_context_data(**kwargs)
        return self.render_to_response(self.request, context)

    async def get_context_data(self, **kwargs):
        context = await super(TemplateView, self).get_context_data(**kwargs)
        context['page_identity'] = self.get_page_identity()
        return context


class DetailViewBase(mx.SingleObjectMixin, BaseView):

    def __init__(self, request):
        super(DetailViewBase, self).__init__(request=request)
        self.object = None

    async def load_object(self):
        pk = self.get_pk()
        self.object = await self.get_object(pk=pk, data_providers=self.request.app.data_providers)

    def get_pk(self):
        return int(self.get_url_param(self.pk_url_kwarg, 0))


class ListViewBase(mx.MultipleObjectMixin, BaseView):

    def __init__(self, request):
        super(ListViewBase, self).__init__(request=request)
        self.object_list = None

    async def load_object_list(self):
        self.object_list = await self.get_object_list(data_providers=self.request.app.data_providers)


class DetailView(mx.MixedResponseMixin, DetailViewBase):
    """
    A base view for displaying a single object    """

    async def get(self, *args, **kwargs):
        await self.load_object()
        context = await self.get_context_data(**kwargs)
        context['page_identity'] = self.get_page_identity()
        content_type = self.get_url_param(self.content_type_name, 'template')
        return self.render_to_response(self.request, context, content_type=content_type)


class ListView(mx.MixedResponseMixin, ListViewBase):

    async def get(self, *args, **kwargs):
        await self.load_object_list()
        context = await self.get_context_data(**kwargs)
        context['page_identity'] = self.get_page_identity()
        content_type = self.get_url_param(self.content_type_name, 'template')
        return self.render_to_response(self.request, context, content_type=content_type)


class JsonView(mx.JsonResponseMixin, mx.ContextMixin, BaseView):
    async def get(self, *args, **kwargs):
        context = await self.get_context_data(**kwargs)
        return self.render_to_response_json(context)


class JsonDetailView(mx.JsonResponseMixin, DetailViewBase):

    async def get(self, *args, **kwargs):
        await self.load_object()
        return self.render_to_response_json(self.object)

    def convert_context_to_json(self, context, **kwargs):
        return jhelp.serialize_entity(context, **kwargs)


class JsonListView(mx.JsonResponseMixin, ListViewBase):

    async def get(self, *args, **kwargs):
        await self.load_object_list()
        return self.render_to_response_json(self.object_list)

    def convert_context_to_json(self, context, **kwargs):
        if self.converter:
            return jhelp.serialize_list(self.converter, context)
        return jhelp.serialize_entity_list(context, **kwargs)


class ProcessFormView(mx.FormMixin, TemplateView):

    """
    A mixin that renders a form on GET and processes it on POST.
    """
    async def get(self, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        return self.render_to_response(self.request, await self.get_context_data(
                form=self.get_form(self.request.app, self.request[SESSION_KEY])
                , **kwargs))

    async def post(self, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form(self.request.app, self.request[SESSION_KEY], await self.request.post())
        is_valid = await form.validate()
        if is_valid:
            return await self.form_valid(form, **kwargs)
        else:
            return await self.form_invalid(form, **kwargs)


class SingleObjectEditView(mx.ActionPostMixin, DetailView):
    async def post(self, *args, **kwargs):
        result = await self.handle_action(self.request, *args, **kwargs)
        return self.render_to_response(request=self.request, result=result)


class SingleJsonObjectEditView(mx.ActionJsonMixin, JsonDetailView):
    async def post(self, *args, **kwargs):
        result = await self.handle_action(self.request, *args, **kwargs)
        return self.render_to_response_json(result)
