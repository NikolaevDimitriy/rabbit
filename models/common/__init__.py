__author__ = 'dimitriy'
from utils.json_helper import serialize_list
from utils.keyvalue_helper import get_key_value_model
from utils.types import KeyValue
from utils.localization import gettext_local as _


def get_days_of_week_json():
    return serialize_list(get_key_value_model, DAYS_OF_WEEK)


def get_days_of_week():
    return [day for day in DAYS_OF_WEEK if day.key != 0]


def get_day_of_week(day_of_week):
    if day_of_week == 1:
        return DAYS_OF_WEEK[7]
    elif day_of_week == 2:
        return DAYS_OF_WEEK[1]
    elif day_of_week == 3:
        return DAYS_OF_WEEK[2]
    elif day_of_week == 4:
        return DAYS_OF_WEEK[3]
    elif day_of_week == 5:
        return DAYS_OF_WEEK[4]
    elif day_of_week == 6:
        return DAYS_OF_WEEK[5]
    elif day_of_week == 7:
        return DAYS_OF_WEEK[6]

DAYS_OF_WEEK = (
    KeyValue(0, _('Undefined'),),
    KeyValue(1, _('Monday'),),
    KeyValue(2, _('Tuesday'),),
    KeyValue(4, _('Wednesday'),),
    KeyValue(8, _('Thursday'),),
    KeyValue(16, _('Friday'),),
    KeyValue(32, _('Saturday'),),
    KeyValue(64, _('Sunday'),),
)
