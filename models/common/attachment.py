__author__ = 'dimitriy'
from enum import Enum
from sqlalchemy import Column, Text, Integer, BigInteger, Boolean, Index
from sqlalchemy.orm import validates
from sqlalchemy_utils import ChoiceType
from models.abstract_models import DocumentModel
from utils.keyvalue_helper import get_key_value_model
from utils.types import KeyValue
from utils.json_helper import serialize_list
from utils.localization import gettext_local as _


def get_attachment_types_json():
    return serialize_list(get_key_value_model, ATTACHMENT_TYPES)

def get_customer_attachment_types_json():
    return serialize_list(get_key_value_model, UNDEFINED_ATTACHMENT_TYPE + CUSTOMER_ATTACHMENTS_TYPES)

UNDEFINED_ATTACHMENT_TYPE = (KeyValue(0, _('Undefined'),),)

CUSTOMER_ATTACHMENTS_TYPES = (
    KeyValue(1, _('Regulations'),), #Устав
    KeyValue(2, _('Registry certificate'),), #Свидетельство о регистрации
    KeyValue(3, _('Contract B2B'),), #Контракт между партнерами
    KeyValue(4, _('EGRUL certificate'),), #Выписка из ЕГРЮЛ
    KeyValue(5, _('Balance (Form №1)'),), #Баланс форма №1
    KeyValue(6, _('P&L form'),), #Отчет о прибылях и убытках
    KeyValue(7, _('CEO order appointing'),), #Приказ о назначении генерального директора
    KeyValue(8, _('Accountant-general order appointing'),), #Приказ о назначении главного бухгалтера
    KeyValue(9, _('T1 ref.'),), #Справка 1 – Т (о численности работников)
    KeyValue(10, _('Office lease'),), #Договор аренды офиса
    KeyValue(11, _('Ownership certificate'),), #Свидетельство о праве собственности на помещение
    KeyValue(12, _('INN certificate'),), #ИНН
    KeyValue(13, _('PTS list'),), #Список птс
    KeyValue(14, _('Tax declaration'),), #Налоговая декларация
    KeyValue(15, _('Revenue report'),), #Информационное письмо о выручке
    KeyValue(16, _('USN notification'),), #Уведомление о применении УСН
    KeyValue(17, _('Power of attorney'),), #Довереность
)

ATTACHMENT_TYPES = UNDEFINED_ATTACHMENT_TYPE + CUSTOMER_ATTACHMENTS_TYPES


class Attachment(DocumentModel):
    __tablename__ = 'attachments'
    _entity_identifier = 21
    type = Column(ChoiceType(ATTACHMENT_TYPES, impl=Integer), nullable=False)
    entity_type = Column(Integer, nullable=False)
    entity_id = Column(BigInteger, nullable=False)
    description = Column(Text)
    file = Column(Text, nullable=False)
    __table_args__ = (Index('entity_uix', "entity_type", "entity_id"), )

    __mapper_args__ = {
        'polymorphic_identity': 'attachments',
        'concrete': True}

    def to_dict(self, **kwargs):
        res = super(Attachment, self).to_dict()
        res['entity_type'] = self.entity_type
        res['entity_id'] = self.entity_id
        res['type'] = self.type
        res['description'] = self.description
        res['file'] = self.file,
        return res

    def from_dict(self, **kwargs):
        super(Attachment, self).from_dict(**kwargs)
        self.entity_type = kwargs['entity_type']
        self.entity_id = kwargs['entity_id']
        self.type = kwargs['type']
        self.description = kwargs['description']
        self.file = kwargs['file']
        return self

    @staticmethod
    def get_default_instance(etype, eid, creator):
        return Attachment(entity_type=etype, entity_id=eid, type=0, description='', creator=creator)

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(Attachment, cls).get_default(**kwargs)
        instance.entity_type = kwargs.get('entity_type', 0)
        instance.entity_id = kwargs.get('entity_id', 0)
        instance.type = kwargs.get('type', 0)
        instance.description = kwargs.get('description', '')
        return instance
