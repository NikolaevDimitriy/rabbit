__author__ = 'dimitriy'

from models import Model
from sqlalchemy import Column, Text, BigInteger, SmallInteger, Boolean, ForeignKey, Index, Numeric
from sqlalchemy.orm import relationship
from models.extensions import GUID


class Country(Model):
    __tablename__ = 'countries'
    _entity_identifier = 23

    name_ru = Column(Text, nullable=False)
    name_en = Column(Text, nullable=False)

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(Country, cls).get_default(**kwargs)
        return instance

    def to_dict(self, **kwargs) -> dict:
        res = super(Country, self).to_dict()
        res['name_ru'] = self.name_ru
        res['name_en'] = self.name_en
        return res

    def from_dict(self, **kwargs)-> 'Country':
        super(Country, self).from_dict(**kwargs)
        self.name_ru = kwargs['name_ru']
        self.name_en = kwargs['name_en']
        return self


class Geo(Model):
    __tablename__ = 'geos'
    _entity_identifier = 24

    global_fias_id = Column(GUID, nullable=False)
    parent_fias_id = Column(GUID, nullable=True)
    parent_id = Column(BigInteger, nullable=True)
    name = Column(Text, nullable=False)
    official_name = Column(Text, nullable=False)
    name_en = Column(Text, nullable=True)
    full_name = Column(Text, nullable=False)
    address_type = Column(Text, nullable=False)
    address_level = Column(SmallInteger, nullable=False)
    is_fias = Column(Boolean, nullable=False)

    country_id = Column(BigInteger, ForeignKey('countries.id'), nullable=False)
    country = relationship(Country, foreign_keys=country_id)

    @property
    def country_prop(self):
        return self.country

    @country_prop.setter
    def country_prop(self, val):
        if val:  # nullable
            self.country = val
            self.country_id = val.id
        else:
            self.country = None
            self.country_id = None

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(Geo, cls).get_default(**kwargs)
        instance.is_fias = False
        instance.country_id = 1
        return instance

    def to_dict(self, **kwargs) -> dict:
        res = super(Geo, self).to_dict()
        res['global_fias_id'] = self.global_fias_id
        res['parent_fias_id'] = self.parent_fias_id
        res['parent_id'] = self.parent_id
        res['name'] = self.name
        res['official_name'] = self.official_name
        res['name_en'] = self.name_en
        res['full_name'] = self.full_name
        res['address_type'] = self.address_type
        res['address_level'] = self.address_level
        res['is_fias'] = self.is_fias
        res['country'] = self.country_prop.to_dict() if self.country_prop else ''

    def from_dict(self, **kwargs)-> 'Geo':
        super(Geo, self).from_dict(**kwargs)
        self.global_fias_id = kwargs['global_fias_id']
        self.parent_fias_id = kwargs['parent_fias_id']
        self.parent_id = kwargs['parent_id']
        self.name = kwargs['name']
        self.official_name = kwargs['official_name']
        self.name_en = kwargs['name_en']
        self.full_name = kwargs['full_name']
        self.address_type = kwargs['address_type']
        self.address_level = kwargs['address_level']
        self.is_fias = kwargs['is_fias']
        if 'country' in kwargs and kwargs['country']:
            self.country_prop = Country().from_dict(**kwargs['country'])
        return self
