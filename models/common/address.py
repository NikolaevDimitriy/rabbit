__author__ = 'dimitriy'
from decimal import Decimal
from models import Model
from sqlalchemy import Column, Text, BigInteger, Integer, Boolean, Index, Numeric
from utils.localization import gettext_local as _


class Address(Model):
    __tablename__ = 'address'
    _entity_identifier = 22

    country_id = Column(BigInteger, nullable=False)
    country_name = Column(Text, nullable=False)
    region_id = Column(BigInteger, nullable=True)
    region_name = Column(Text, nullable=True)
    district_id = Column(BigInteger, nullable=True)
    district_name = Column(Text, nullable=True)
    city_id = Column(BigInteger, nullable=False)
    city_name = Column(Text, nullable=False)
    street = Column(Text, nullable=True)
    building = Column(Text, nullable=True)
    apartment = Column(Text, nullable=True)
    underground_station_id = Column(Integer, nullable=True)
    underground_station_name = Column(Text, nullable=True)
    latitude = Column(Numeric(precision=8, scale=6), nullable=True)
    longitude = Column(Numeric(precision=9, scale=6), nullable=True)

    @property
    def full_address(self):
        if self.country_name:
            full = self.country_name
        else:
            return ''#_('Press button and input address →')
        if self.region_name:
            full += ', ' + self.region_name
        if self.district_name:
            full += ', ' + self.district_name
        if self.city_name:
            full += ', ' + self.city_name
        if self.street:
            full += ', ' + self.street
        if self.building:
            full += ', ' + self.building
        if self.apartment:
            full += ', ' + self.apartment
        return full

    def to_dict(self, **kwargs):
        res = super(Address, self).to_dict()
        res['country_id'] = self.country_id
        res['country_name'] = self.country_name
        res['region_id'] = self.region_id
        res['region_name'] = self.region_name
        res['district_id'] = self.district_id
        res['district_name'] = self.district_name
        res['city_id'] = self.city_id
        res['city_name'] = self.city_name
        res['street'] = self.street
        res['building'] = self.building
        res['apartment'] = self.apartment
        res['underground_station_id'] = self.underground_station_id
        res['underground_station_name'] = self.underground_station_name
        res['latitude'] = float(self.latitude)
        res['longitude'] = float(self.longitude)
        return res

    def from_dict(self, **kwargs):
        super(Address, self).from_dict(**kwargs)
        self.country_id = kwargs['country_id']
        self.country_name = kwargs['country_name']
        self.region_id = kwargs['region_id']
        self.region_name = kwargs['region_name']
        self.district_id = kwargs['district_id']
        self.district_name = kwargs['district_name']
        self.city_id = kwargs['city_id']
        self.city_name = kwargs['city_name']
        self.street = kwargs['street']
        self.building = kwargs['building']
        self.apartment = kwargs['apartment']
        self.underground_station_id = kwargs['underground_station_id']
        self.underground_station_name = kwargs['underground_station_name']
        self.latitude = Decimal(kwargs['latitude'])
        self.longitude = Decimal(kwargs['longitude'])
        return self

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(Address, cls).get_default(**kwargs)
        instance.country_id = 1
        instance.country_name = str(_('Russia'))
        instance.region_id = 0
        instance.region_name = ''
        instance.district_id = 0
        instance.district_name = ''
        instance.city_id = 0
        instance.city_name = ''
        instance.street = ''
        instance.building = ''
        instance.apartment = ''
        instance.latitude = 0
        instance.longitude = 0
        return instance

    def is_equal_to(self, other):
        return self.country_id == other.country_id and self.country_name == other.country_name and \
            self.region_id == other.region_id and self.region_name == other.region_name and \
            self.district_id == other.district_id and self.district_name == other.district_name and \
            self.street == other.street and self.building == other.building and self.apartment == other.apartment and \
            self.underground_station_id == other.underground_station_id and \
            self.underground_station_name == other.underground_station_name and \
            self.latitude == other.latitude and self.longitude == other.longitude
