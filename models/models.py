__author__ = 'dimitriy'
from models import Model
from models.account import user
from models.account import group
from models.common import attachment, address, geo
from models.customer import customer
from utils.localization import gettext_local as _

NUM_ENTITIES = {
    0: ('Undefined', _('Undefined'), Model),
    # account 1-20
    1: ('User', _('User'), user.User),
    2: ('Group', _('Group'),  group.Group),
    # common 21-60
    21: ('Attachment', _('Attachment'), attachment.Attachment),
    22: ('Address', _('Address'), address.Address),
    # customer 61-110
    61: ('Customer', _('Customer'), customer.Customer)
}
ENTITIES_NUM = {val[2]: (val[0], val[1], idx) for idx, val in NUM_ENTITIES.items()}
