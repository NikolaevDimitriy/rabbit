__author__ = 'dimitriy'
from sqlalchemy import Column, Text, BigInteger, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship
from models import Base, Model

user_group_table = Table('user_group', Base.metadata,
                         Column('user_id', BigInteger, ForeignKey('users.id'), nullable=False),
                         Column('group_id', BigInteger, ForeignKey('groups.id'), nullable=False)
                         )


class Group(Model):
    __tablename__ = 'groups'
    _entity_identifier = 2
    
    name = Column(Text, nullable=False)
    is_global = Column(Boolean, nullable=False, server_default='false')
    users = relationship('User', secondary=user_group_table, backref="groups")

    def to_dict(self, **kwargs) -> dict:
        res = super(Group, self).to_dict()
        res.name = self.name
        return res

    def from_dict(self, **kwargs) -> 'Group':
        super(Group, self).from_dict(**kwargs)
        self.name = kwargs['name']
        return self
