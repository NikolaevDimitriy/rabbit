__author__ = 'dimitriy'
from datetime import datetime
from sqlalchemy import Column, Text, DateTime, Boolean, BigInteger, Integer, ForeignKey
from sqlalchemy.orm import validates, relationship
from models import Model
from models.extensions import utcnow
from utils.datetime_helper import date_to_str_safe
from utils.parsers import get_safe_date
from utils.import_cache import model_cache
from sqlalchemy_utils import TimezoneType
import pytz
from tzlocal import get_localzone


class User(Model):
    __tablename__ = 'users'
    _entity_identifier = 1

    email = Column(Text, unique=True, nullable=False)
    phone = Column(Text, unique=True, nullable=False)
    pwd = Column(Text, nullable=False)
    first_name = Column(Text, nullable=False)
    last_name = Column(Text, nullable=False)
    timezone = Column(TimezoneType(backend='pytz'), nullable=False)
    reg_date = Column(DateTime(), nullable=False, default=utcnow())
    role = Column(Integer, nullable=False, server_default='0')
    is_active = Column(Boolean, nullable=False, server_default='false')

    customer_id = Column(BigInteger, ForeignKey('customers.id'), nullable=True)
    customer = relationship("Customer", back_populates="users", foreign_keys=customer_id)

    @property
    def customer_prop(self):
        return self.customer

    @customer_prop.setter
    def customer_prop(self, val):
        if val:  # nullable
            self.customer = val
            self.customer_id = val.id
        else:
            self.customer = None
            self.customer_id = None

    def __repr__(self):
        return '<User %r %r %r>' % (self.email, self.first_name, self.last_name,)

    def get_name(self):
        return '%s %s' % (self.first_name, self.last_name,)

    @validates('email')
    def validate_email(self, key, email):
        if email is not None:
            assert '@' in email
        return email

    def to_dict(self, minimal=False, **kwargs) -> dict:
        res = super(User, self).to_dict()
        res['id'] = self.id
        res['email'] = self.email
        res['first_name'] = self.first_name
        res['last_name'] = self.last_name
        res['timezone'] = str(self.timezone) if self.timezone else ''
        res['phone'] = self.phone
        res['role'] = self.role
        if not minimal:
            res['customer'] = self.customer_prop.to_dict() if self.customer_prop else ''
            res['is_active'] = self.is_active
            res['reg_date'] = date_to_str_safe(self.reg_date)
            if kwargs.get('include_pwd', True):
                res['pwd'] = self.pwd

        return res

    def from_dict(self, **kwargs) -> 'User':
        super(User, self).from_dict(**kwargs)
        self.email = kwargs.get('email', None)
        self.phone = kwargs.get('phone', None)
        self.pwd = kwargs.get('pwd', None)
        self.is_active = kwargs.get('is_active', True)
        self.first_name = kwargs.get('first_name', '')
        self.last_name = kwargs.get('last_name', '')
        self.timezone = pytz.timezone(kwargs['timezone']) if kwargs['timezone'] else None
        self.reg_date = get_safe_date(kwargs.get('reg_date', ''))
        self.role = kwargs.get('role', 0)
        if 'customer' in kwargs and kwargs['customer']:
            customer_cls = model_cache.get_model_class('models.customer.customer', 'Customer')
            self.customer_prop = customer_cls().from_dict(**kwargs['customer']) if customer_cls else None
        return self

    def get_redis_key(self):
        if self.is_new:
            return None
        return 'user:%d' % (self.id,)

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(User, cls).get_default(**kwargs)
        instance.email = kwargs.get('email', None)
        instance.phone = kwargs.get('phone', None)
        instance.pwd = kwargs.get('pwd', None)
        instance.first_name = kwargs.get('first_name', None)
        instance.last_name = kwargs.get('last_name', None)
        instance.is_active = kwargs.get('is_active', False)
        instance.reg_date = kwargs.get('reg_date',  datetime.utcnow())
        instance.timezone = kwargs.get('timezone', get_localzone())
        instance.role = 0
        if 'customer' in kwargs and kwargs['customer']:
            instance.customer_prop = kwargs['customer']
        return instance
