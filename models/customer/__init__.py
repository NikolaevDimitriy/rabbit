__author__ = 'dimitriy'
from utils.keyvalue_helper import get_key_value_model
from utils.types import KeyValue
from utils.json_helper import serialize_list
from utils.localization import gettext_local as _

CUSTOMER_TYPES = (
    KeyValue(0, _('Undefined')),
    KeyValue(1, _('Legal entity')),
    KeyValue(2, _('Individual')),
)

CUSTOMER_STATUSES = (
    KeyValue(0, _('Undefined')),
    KeyValue(1, _('Active')),
    KeyValue(2, _('Blocked')),
)

CUSTOMER_TYPES_OF_OWNERSHIP = (
    KeyValue(0, _('Undefined')),
    KeyValue(1, _('Individual')),  # person
    KeyValue(2, _('OOO')),
    KeyValue(3, _('Individual Entrepreneur')),
    KeyValue(4, _('OAO')),
    KeyValue(5, _('АO')),
    KeyValue(6, _('ZАO')),
    KeyValue(7, _('PBOUL')),
    KeyValue(8, _('TOO')),
    KeyValue(9, _('AKOT')),
    KeyValue(10, _('SP')),
    KeyValue(11, _('SPD')),
    KeyValue(12, _('KX')),
    KeyValue(13, _('NP')),
    KeyValue(14, _('NPP')),
    KeyValue(15, _('ODO')),
    KeyValue(16, _('PK')),
    KeyValue(17, _('FLP')),
    KeyValue(18, _('ChP')),
    KeyValue(255, _('Other')),
)
DICT_CUSTOMER_TYPES_OF_OWNERSHIP = [get_key_value_model(x) for x in CUSTOMER_TYPES_OF_OWNERSHIP]


def get_org_ownership_types_json():
    return serialize_list(get_key_value_model, CUSTOMER_TYPES_OF_OWNERSHIP)