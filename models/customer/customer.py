__author__ = 'dimitriy'
from datetime import datetime
from marshmallow import Schema, fields
from sqlalchemy import Column, Text, Integer, BigInteger, SmallInteger, DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy_utils import ChoiceType
from models.abstract_models import DocumentModel
from models import customer as defaults
from models import common as common_defaults
from models.common.address import Address
from models.account.user import User
from models.extensions import utcnow
from utils.datetime_helper import date_to_str_safe
from utils.parsers import get_safe_date
from utils import keyvalue_helper
from utils import flags_helper


class LegalEntityRequisitesNames:
    type_of_ownership = 'type_of_ownership'
    register_date = 'register_date'
    bank_name = 'bank_name'
    current_account = 'current_account'
    correspondent_account = 'correspondent_account'
    inn = 'inn'
    kpp = 'kpp'
    okpo = 'okpo'
    oktmo = 'oktmo'
    ogrn = 'ogrn'
    bik = 'bik'


class IndividualRequisitesNames:
    passport_id = 'passport_id'
    issue_date = 'issue_date'
    first_name = 'first_name'
    patronymic = 'patronymic'
    last_name = 'last_name'
    birth_date = 'birth_date'
    sex = 'sex'


class Customer(DocumentModel):
    __tablename__ = 'customers'
    _entity_identifier = 61

    name = Column(Text, nullable=False)
    type = Column(ChoiceType(defaults.CUSTOMER_TYPES, impl=SmallInteger()), nullable=False, server_default='0')
    status = Column(ChoiceType(defaults.CUSTOMER_STATUSES, impl=SmallInteger()), nullable=False, server_default='0')
    phone = Column(Text, nullable=False)
    requisites = Column(JSONB, nullable=False)

    actual_address_id = Column(BigInteger, ForeignKey('address.id'), nullable=False)
    actual_address = relationship(Address, foreign_keys=actual_address_id)

    legal_address_id = Column(BigInteger, ForeignKey('address.id'), nullable=False)
    legal_address = relationship(Address, foreign_keys=legal_address_id)

    manager_id = Column(BigInteger, ForeignKey('users.id'), nullable=False)
    manager = relationship(User, foreign_keys=manager_id)

    users = relationship(User, back_populates="customer", foreign_keys=User.customer_id)

    __mapper_args__ = {
        'polymorphic_identity': 'customers',
        'concrete': True}

    def __repr__(self):
        return '<Customer %r id = %d type = %d>' % (self.name, self.id, self.type,)

    @property
    def manager_prop(self):
        return self.manager

    @manager_prop.setter
    def manager_prop(self, val: User):
        self.manager = val
        self.manager_id = val.id

    @property
    def legal_address_prop(self):
        return self.legal_address

    @legal_address_prop.setter
    def legal_address_prop(self, val: User):
        self.legal_address = val
        self.legal_address_id = val.id

    @property
    def actual_address_prop(self):
        return self.actual_address

    @actual_address_prop.setter
    def actual_address_prop(self, val: User):
        self.actual_address = val
        self.actual_address_id = val.id

    def to_dict(self, minimal=False, **kwargs) -> dict:
        res = super(Customer, self).to_dict()
        res['name'] = self.name
        if not minimal:
            res['type'] = self.type
            res['status'] = self.status
            res['requisites'] = self.requisites
            res['phone'] = self.phone
            res['actual_address'] = self.actual_address.to_dict() if self.actual_address else None
            res['legal_address'] = self.legal_address.to_dict() if self.legal_address else None
            res['manager'] = self.manager_prop.to_dict(minimal=True) if self.manager_prop else {}
        return res

    def from_dict(self, minimal=False, **kwargs) -> 'Customer':
        super(Customer, self).from_dict(**kwargs)
        self.name = kwargs['name']
        if not minimal:
            self.type = kwargs.get('type', 0)
            self.status = kwargs.get('status', 0)
            self.requisites = kwargs.get('requisites', None)
            self.phone = kwargs.get('phone', None)
            if 'actual_address' in kwargs and kwargs['actual_address']:
                self.actual_address_prop = Address().from_dict(**kwargs['actual_address'])
            if 'legal_address' in kwargs and kwargs['legal_address']:
                self.legal_address_prop = Address().from_dict(**kwargs['legal_address'])
            if 'manager' in kwargs and kwargs['manager']:
                self.manager_prop = User().from_dict(**kwargs['manager'])
        return self

    @classmethod
    def get_default(cls, **kwargs):
        instance = super(Customer, cls).get_default(**kwargs)
        # instance.register_date = kwargs.get('register_date', datetime.utcnow())
        manager = kwargs.get('user', None)
        instance.legal_address_prop = Address.get_default(id=0)
        instance.actual_address_prop = Address.get_default(id=0)
        if manager is not None:
            instance.manager_prop = manager
        return instance

    def get_redis_key(self):
        return 'customer:%d' % (self.id,)

    def is_individual(self):
        return self.type == 2
