__author__ = 'dimitriy'

import abc
import datetime
from sqlalchemy import Column, BigInteger, DateTime, ForeignKey
from sqlalchemy.ext.declarative import AbstractConcreteBase, declared_attr
from sqlalchemy.orm import relationship
from models import Model
from models.account.user import User
from models.extensions import utcnow
from utils.datetime_helper import date_to_str_safe
from utils.parsers import get_safe_date


class DocumentModel(AbstractConcreteBase, Model):
    # __abstract_ = True

    created_datetime = Column(DateTime(), nullable=False, default=utcnow())
    deleted_datetime = Column(DateTime())

    @declared_attr
    def creator_id(cls):
        return Column(BigInteger, ForeignKey(User.id), nullable=False)

    @declared_attr
    def creator(cls):
        return relationship(User, foreign_keys='{}.{}'.format(cls.__name__, 'creator_id'))

    __metaclass__ = abc.ABCMeta

    @property
    def creator_prop(self):
        return self.creator

    @creator_prop.setter
    def creator_prop(self, val: User):
        self.creator = val
        self.creator_id = val.id

    @abc.abstractmethod
    def to_dict(self, **kwargs) -> dict:
        return {
            'id': self.id,
            'entity_identifier': self.entity_identifier,
            'deleted': self.deleted,
            'created_datetime': date_to_str_safe(self.created_datetime),
            'deleted_datetime': date_to_str_safe(self.deleted_datetime),
            'creator': self.creator.to_dict(minimal=True) if self.creator else {}
        }

    @abc.abstractmethod
    def from_dict(self, **kwargs):
        super(DocumentModel, self).from_dict(**kwargs)
        self.created_datetime = get_safe_date(kwargs['created_datetime'])
        self.deleted_datetime = get_safe_date(kwargs['deleted_datetime'])
        if kwargs['creator']:
            self.creator_prop = User().from_dict(**kwargs['creator'])
        return self

    @abc.abstractclassmethod
    def get_default(cls, **kwargs):
        instance = super(DocumentModel, cls).get_default(**kwargs)
        instance.created_datetime = kwargs.get('created_datetime', datetime.datetime.utcnow())
        creator = kwargs.get('user', None)
        if creator is not None:
            instance.creator_prop = creator
            # instance.creator_id = creator.id
        return instance
