__author__ = 'dimitriy'

import abc
from sqlalchemy import Column, String, BigInteger, Boolean
from sqlalchemy.orm import validates
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from utils.json_helper import serialize_entity


class ModelMeta(DeclarativeMeta):

    @property
    def tb(cls):
        return cls.__table__

    @property
    def tbc(cls):
        return cls.__table__.c


Base = declarative_base(metaclass=ModelMeta)


class Model(Base):
    __abstract__ = True
    _entity_identifier = 0

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    deleted = Column(Boolean, nullable=False, server_default='false')

    def __init__(self, **kwargs):
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError(
                    "%r is an invalid keyword argument for %s" %
                    (k, cls_.__name__))
            setattr(self, k, kwargs[k])

    @property
    def get_class(self):
        return type(self)

    @property
    def entity_identifier(self):
        return self._entity_identifier

    @property
    def safe_id(self):
        return self.id if self.id is not None else 0

    @property
    def is_new(self):
        return self.safe_id == 0

    @abc.abstractmethod
    def to_dict(self, **kwargs) -> dict:
        return {
            'id': self.id,
            'entity_identifier': self.entity_identifier,
            'deleted': self.deleted,
        }

    @abc.abstractmethod
    def from_dict(self, **kwargs):
        self.id = kwargs['id']
        self.deleted = kwargs['deleted']
        return self

    @property
    def json(self):
        return serialize_entity(self)

    @abc.abstractclassmethod
    def get_default(cls, **kwargs):
        instance = cls(id=kwargs.get('id', None), deleted=kwargs.get('deleted', False))
        return instance
