__author__ = 'dimitriy'

from sqlalchemy.sql import expression
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.types import DateTime
from sqlalchemy.types import TypeDecorator
from sqlalchemy.dialects.postgresql import UUID
import uuid


class utcnow(expression.FunctionElement):
    type = DateTime()


@compiles(utcnow)
def pg_utcnow(element, compiler, **kw):
    return "now() at time zone 'utc'"


class GUID(TypeDecorator):
    """Platform-dependent GUID type.

    Uses Postgresql's UUID type.

    """

    impl = UUID

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        return str(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            return uuid.UUID(value)
