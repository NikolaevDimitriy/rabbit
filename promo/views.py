__author__ = 'dimitriy'

from web_core.views import TemplateView
from auth.contrib import get_login_form


class IndexView(TemplateView):
    template_name = 'promo/index.html'
    page_identity = 'promo_index'

    async def get_context_data(self, **kwargs):
        context = await super(IndexView, self).get_context_data(**kwargs)
        context['form'] = get_login_form(request=self.request)
        return context
