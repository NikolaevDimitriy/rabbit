__author__ = 'dimitriy'

from web_core.application import AIOApplication
from promo.views import IndexView


class AioPromoApplication(AIOApplication):

    def name(self):
        return 'promo'

    def get_aio_urls(self):
        url_patterns = (
            ('', IndexView, 'index'),
        )
        return url_patterns

aio_application = AioPromoApplication()
