__author__ = 'dimitriy'

import pytest
from decimal import Decimal
from storages import create_data_providers
from storages.account import UserStorage
from storages.customer import CustomerStorage
from storages.common import AddressStorage
from auth.model import AuthUser
from models.customer.customer import Customer, LegalEntityRequisitesNames
from models.common.address import Address


@pytest.mark.run_loop
async def test_customer_add_get_update_delete(loop, user):
    engine = await create_data_providers(loop)

    u_storage = UserStorage(engine)

    res = await u_storage.save(user, (AuthUser.phone, AuthUser.email.key, AuthUser.role,
                                      'first_name', 'last_name', 'pwd', 'reg_date', 'timezone'))
    customer = Customer.get_default(user=user)
    customer.name = 'test_customer'
    customer.type = 1
    customer.status = 1
    customer.phone = '+7(905)273-56-99'
    customer.requisites = {
        LegalEntityRequisitesNames.bank_name: 'Tinkoff',
        LegalEntityRequisitesNames.bik: '53761253',
        LegalEntityRequisitesNames.inn: '7234628734682'
    }

    address = Address.get_default()

    address.region_id = 131378
    address.region_name = 'Регион'
    address.district_id = 43534
    address.district_name = 'Район'
    address.city_id = 234234
    address.city_name = 'Город'
    address.street = 'Улица'
    address.building = '13 A'
    address.apartment = '204'
    address.latitude = Decimal('34.879874')
    address.longitude = Decimal('-113.787987')

    address_storage = AddressStorage(engine)

    res = await address_storage.save(address)

    assert res
    assert address.id > 0
    customer.legal_address = address
    customer.legal_address_id = address.id

    customer.actual_address = address
    customer.actual_address_id = address.id

    customer_storage = CustomerStorage(engine)
    res = await customer_storage.save(customer)

    assert res
    assert customer.id > 0

    user.customer_prop = customer

    await u_storage.save(user, ('customer_id',))

    loaded_customer = await customer_storage.get(customer.id)

    assert loaded_customer.requisites[LegalEntityRequisitesNames.inn] == customer.requisites[
        LegalEntityRequisitesNames.inn]
    assert loaded_customer.requisites[LegalEntityRequisitesNames.bank_name] == customer.requisites[
        LegalEntityRequisitesNames.bank_name]
    assert loaded_customer.requisites[LegalEntityRequisitesNames.bik] == customer.requisites[
        LegalEntityRequisitesNames.bik]

    # clear db
    user.customer_prop = None
    await u_storage.save(user, ('customer_id',))
    await customer_storage.delete(customer)
    await u_storage.delete(user)
    await address_storage.delete(address)

    engine.close()
    await  engine.wait_closed()
