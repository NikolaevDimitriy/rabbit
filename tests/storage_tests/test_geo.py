__author__ = 'dimitriy'

import pytest
from storages import create_data_providers
from storages.geo import CountryStorage


@pytest.mark.run_loop
async def test_countries(loop):
    engine = await create_data_providers(loop)

    geo_storage = CountryStorage(engine)

    res = await geo_storage.get_list(order_by='name_ru')

    assert len(res) > 0

    engine.close()
    await engine.wait_closed()
