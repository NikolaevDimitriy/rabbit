__author__ = 'dimitriy'

import pytest
from auth.model import AuthUser
from storages.base import PgBaseStorage
from storages.utility_storage import PgUniqueChecker, Condition
from storages import create_data_providers, DataProviders

class TestBase(PgBaseStorage):
    model = AuthUser


class TestBaseUnique(PgUniqueChecker):
    model = AuthUser


def test_get_args_default(user):
    dt = DataProviders(pg_db=None)
    storage = TestBase(dt)
    args = storage.get_args_add_or_update(user)
    assert args['phone'] == user.phone
    assert args['email'] == user.email
    assert args['first_name'] == user.first_name
    assert args['last_name'] == user.last_name
    assert args['role'] == user.role
    assert args['is_active'] == user.is_active
    assert args['pwd'] == user.pwd
    assert args['reg_date'] == user.reg_date
    assert args['deleted'] == user.deleted
    assert len(args) == len(storage.model.tbc) - 1  # minus pk


def test_get_args_by_names(user):
    dt = DataProviders(pg_db=None)
    storage = TestBase(dt)
    fields = ('deleted', AuthUser.phone, AuthUser.email.name)
    args = storage.get_args_add_or_update(user, fields)
    assert args['phone'] == user.phone
    assert args['email'] == user.email
    assert args['deleted'] == user.deleted
    assert len(args) == len(fields)  # minus pk


@pytest.mark.run_loop
async def test_add_get_update_delete(loop, user):
    engine = await create_data_providers(loop)

    storage = TestBase(engine)

    await storage.save(user, ('phone', 'email', 'first_name', 'last_name', 'pwd', 'timezone', 'role'))
    assert user.id > 0
    saved = await storage.get(user.id)
    assert saved.phone == user.phone
    assert saved.email == user.email
    assert saved.first_name == user.first_name
    assert saved.last_name == user.last_name
    assert saved.pwd == user.pwd
    assert saved.role == user.role

    saved.email = 'new_test_email@mail.mail'
    saved.is_active = True
    await storage.save(saved, ('email', 'is_active'))

    user1 = await storage.get(saved.id)
    assert user1.email == saved.email
    assert user1.is_active

    await storage.delete(user1)

    user2 = await storage.get(user1.id)

    assert user2 is None

    engine.close()
    await engine.wait_closed()


@pytest.mark.run_loop
async def test_check_unique(loop, user):
    engine = await create_data_providers(loop)

    storage = TestBase(engine)

    await storage.save(user, ('phone', 'email', 'first_name', 'last_name', 'pwd', 'timezone', 'role'))
    assert user.id > 0

    checker = TestBaseUnique(engine)

    # unique excluding me
    res = await checker.check_exists([Condition(AuthUser.email.name, [AuthUser.tbc.email == user.email,
                                                                      AuthUser.tbc.id != user.id]),
                                      Condition(AuthUser.phone.name, [AuthUser.tbc.phone == user.phone,
                                                                      AuthUser.tbc.id != user.id])])

    assert not res[AuthUser.email.name]
    assert not res[AuthUser.phone.name]

    #  simple check unique
    user.email = 'test1@test.mail'
    user.phone = '+79052735698'

    res = await checker.check_exists([Condition(AuthUser.email.name, AuthUser.tbc.email == user.email),
                                      Condition(AuthUser.phone.name, AuthUser.tbc.phone == user.phone)])

    assert not res[AuthUser.email.name]
    assert not res[AuthUser.phone.name]

    # another existed user
    user.email = 'shurup@mail.mail'
    user.phone = '+79052439987'

    res = await checker.check_exists([Condition(AuthUser.email.name, [AuthUser.tbc.email == user.email,
                                                                      AuthUser.tbc.id != user.id]),
                                      Condition(AuthUser.phone.name, [AuthUser.tbc.phone == user.phone,
                                                                      AuthUser.tbc.id != user.id])])

    assert res[AuthUser.email.name]
    assert res[AuthUser.phone.name]

    await storage.delete(user)

    engine.close()
    await engine.wait_closed()


async def test_insert_bulk(loop, users):
    engine = await create_data_providers(loop)

    storage = TestBase(engine)

    await storage.bulk_save(users, ('phone', 'email', 'first_name', 'last_name', 'pwd', 'timezone', 'role'))

    engine.close()
    await engine.wait_closed()
