__author__ = 'dimitriy'

import pytest
import aiomysql
import asyncio
import settings as ls
from storages.geo import CountryStorage


@pytest.mark.run_loop
async def test_search(loop):
    pool = await aiomysql.create_pool(host=ls.SEARCH_ENGINE_HOST,
                                      port=ls.SEARCH_ENGINE_PORT,
                                      loop=loop,
                                      charset='utf8', use_unicode=True)
    res = []
    async with pool.get() as conn:
        async with conn.cursor() as cur:
            await cur.execute("SELECT * FROM cities WHERE MATCH('моск*') and address_level = 1 ORDER BY address_level ASC;")
            rows = await cur.fetchall()
            for row in rows:
                res.append(row)
    assert len(res) > 0
    pool.close()
    await pool.wait_closed()

