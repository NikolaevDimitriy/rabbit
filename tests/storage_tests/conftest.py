__author__ = 'dimitriy'

import pytest
from auth.model import AuthUser


@pytest.fixture
def user():
    instance = AuthUser.get_default(email='test@mail.mail', phone='+79052735699',
                                    first_name='test_first_name', last_name='test_last_name', role=0)
    instance.set_password('1234567890')
    return instance


@pytest.fixture
def users():
    instances = [AuthUser.get_default(email='test{0}@mail.mail'.format(i), phone='+7905273569{0}'.format(i),
                                      first_name='test_first_name', last_name='test_last_name',
                                      role=0) for i in range(9)]
    for instance in instances:
        instance.set_password('1234567890')
    return instances


@pytest.fixture
def test_file_context():
    return '../files/', 'test_entity/', '1', 'test', bytes([32, 10, 209, 100])

@pytest.fixture
def test_sftpfile_context():
    return './test_entity/', '1', 'test', bytes([32, 10, 209, 100])
