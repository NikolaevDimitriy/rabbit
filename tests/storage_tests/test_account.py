__author__ = 'dimitriy'

import pytest
import asyncio
import ujson as json
from datetime import datetime
from auth.model import AuthUser
from storages import create_data_providers
from storages.account import UserStorage


@pytest.mark.run_loop
async def test_add_get_update_delete(loop, user):
    dp = await create_data_providers(loop)

    storage = UserStorage(data_providers=dp)

    res = await storage.save(user, (AuthUser.email.key, AuthUser.phone, 'first_name', 'last_name',
                                    'pwd', 'reg_date', 'timezone', 'role'))
    assert res

    @asyncio.coroutine
    def load_user(gine, q):
        with (yield from gine.pg_master) as conn:
            res = yield from conn.execute(q)
            row = yield from res.fetchone()
            if row is not None:
                return AuthUser(**dict(row))
            return None

    query = AuthUser.tb.select().where(AuthUser.tbc.id == user.id)

    loaded_u = await load_user(dp.pg_db, query)

    assert loaded_u.id == user.id
    assert loaded_u.email == user.email
    assert loaded_u.phone == user.phone
    assert loaded_u.first_name == user.first_name
    assert loaded_u.last_name == user.last_name
    assert loaded_u.check_password(user._password)
    assert loaded_u.deleted is not None and not loaded_u.deleted
    assert loaded_u.is_active is not None and not loaded_u.is_active
    assert loaded_u.role is not None and not loaded_u.role
    assert loaded_u.reg_date is not None and loaded_u.reg_date < datetime.utcnow()

    with (await dp.redis) as r:
        res = await r.get(user.get_redis_key())
        loadedr_u = AuthUser().from_dict(**json.loads(res))

    test_load = await storage.get(loaded_u.id)

    assert loadedr_u.id == loaded_u.id == test_load.id
    assert loadedr_u.phone == loaded_u.phone == test_load.phone
    assert loadedr_u.email == loaded_u.email == test_load.email
    assert loadedr_u.first_name == loaded_u.first_name == test_load.first_name
    assert loadedr_u.last_name == loaded_u.last_name == test_load.last_name
    assert loadedr_u.pwd == loaded_u.pwd == test_load.pwd == user.pwd
    assert loadedr_u.deleted == loaded_u.deleted == test_load.deleted
    assert loadedr_u.is_active == loaded_u.is_active == test_load.is_active
    assert loadedr_u.role == loaded_u.role == test_load.role
    assert loadedr_u.reg_date == loaded_u.reg_date == test_load.reg_date

    loaded_u.email = 'new@mail.mail'

    await storage.save(loaded_u, (AuthUser.email,))

    loaded2_u = await load_user(dp.pg_db, query)

    with (await dp.redis) as r:
        res = await r.get(user.get_redis_key())
        loadedr2_u = AuthUser().from_dict(**json.loads(res))

    assert loaded2_u.email == loadedr2_u.email == loaded_u.email

    await storage.delete(loaded2_u)

    loaded3_u = await load_user(dp.pg_db, query)
    assert loaded3_u is None

    with (await dp.redis) as r:
        res = await r.get(user.get_redis_key())

    assert res is None

    insta = await storage.get(loaded_u.id)
    assert insta is None

    dp.close()
    await dp.wait_closed()
