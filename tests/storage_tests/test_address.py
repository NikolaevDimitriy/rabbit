__author__ = 'dimitriy'

import pytest
from decimal import Decimal
from storages import create_data_providers
from models.common.address import Address
from storages.common import AddressStorage


@pytest.mark.run_loop
async def test_address_add_get_update_delete(loop, user):
    engine = await create_data_providers(loop)
    address = Address.get_default()

    address.region_id = 131378
    address.region_name = 'Регион'
    address.district_id = 43534
    address.district_name = 'Район'
    address.city_id = 234234
    address.city_name = 'Город'
    address.street = 'Улица'
    address.building = '13'
    address.apartment = '204'
    address.latitude = Decimal('34.879874')
    address.longitude = Decimal('-113.787987')

    storage = AddressStorage(engine)

    res = await storage.save(address)
    assert res
    assert address.id > 0

    new_address = await storage.get(address.id)

    assert new_address.id == address.id
    assert new_address.country_id == address.country_id
    assert new_address.country_name == address.country_name
    assert new_address.region_id == address.region_id
    assert new_address.region_name == address.region_name
    assert new_address.district_id == address.district_id
    assert new_address.district_name == address.district_name
    assert new_address.city_id == address.city_id
    assert new_address.city_name == address.city_name
    assert new_address.street == address.street
    assert new_address.building == address.building
    assert new_address.apartment == address.apartment
    assert new_address.latitude == address.latitude
    assert new_address.longitude == address.longitude

    new_address.street = 'Улица1'
    new_address.district_id = 43533
    new_address.district_name = 'Район1'

    res = await storage.save(new_address, fields=('street', 'district_id', 'district_name'))
    assert res

    updateted_address = await storage.get(new_address.id)

    assert new_address.street == updateted_address.street
    assert new_address.district_id == updateted_address.district_id
    assert new_address.district_name == updateted_address.district_name

    res = await storage.delete(updateted_address)

    assert res

    deleted_address = await storage.get(updateted_address.id)

    assert deleted_address is None

    engine.close()
    await engine.wait_closed()
