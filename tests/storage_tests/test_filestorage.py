__author__ = 'dimitriy'
import pytest
from storages.file_storage import LocalFileSystemStorage, SftpFileStorage
import os


@pytest.mark.run_loop
async def test_file_insert(loop, test_file_context):

    storage = LocalFileSystemStorage(loop)
    filename = test_file_context[3]
    current_dir = os.getcwd()
    print(current_dir)
    dir = os.path.join(current_dir, test_file_context[0], test_file_context[1], test_file_context[2])
    full_path = os.path.join(dir, filename)
    await storage.save(dir, filename, test_file_context[4])
    f = open(full_path, 'rb+')
    context = f.read()

    assert test_file_context[4] == context


@pytest.mark.run_loop
async def test_file_get(loop, test_file_context):

    storage = LocalFileSystemStorage(loop)
    filename = test_file_context[3]
    current_dir = os.getcwd()
    print(current_dir)
    dir = os.path.join(current_dir, test_file_context[0], test_file_context[1], test_file_context[2])
    full_path = os.path.join(dir, filename)
    context = await storage.get_file(full_path)

    assert test_file_context[4] == context


@pytest.mark.run_loop
async def test_sftpfile_insert(loop, test_sftpfile_context):

    storage = SftpFileStorage(loop, host='localhost', port=22, user='attacher', pwd='pdb89')
    filename = test_sftpfile_context[2]
    dir = os.path.join(test_sftpfile_context[0], test_sftpfile_context[1])
    await storage.save(dir, filename, test_sftpfile_context[3])

    full_path = os.path.join(dir, filename)
    context = await storage.get_file(full_path)

    assert context == test_sftpfile_context[3]




