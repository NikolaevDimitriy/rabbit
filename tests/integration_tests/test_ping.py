import pytest


@pytest.mark.run_loop
async def test_ping(cli):
    resp = await cli.get('/test')
    assert resp.status == 200
    assert await resp.text() == 'ok'


@pytest.mark.run_loop
async def test_main_promo(cli):
    resp = await cli.get('/')
    assert resp.status == 200

