__author__ = 'dimitriy'
import json


async def test_get_my_customer_empty(logged_cli_no_customer):

    res = await logged_cli_no_customer.get('api/my_customer')
    resp = await res.json()
    assert res.status == 200
    assert resp.get('id') is None


async def test_individual_customer(logged_cli, individual_customer_data):
    resp = await logged_cli.post('api/my_individual_customer', data=json.dumps(individual_customer_data[0]))
    new_customer_resp = await resp.json()
    assert resp.status == 200

    res = await logged_cli.get('api/my_customer')
    resp = await res.json()
    assert res.status == 200
    assert resp['type'] == individual_customer_data[0]['type']
    assert resp['requisites']['first_name'] == individual_customer_data[0]['requisites']['first_name']


