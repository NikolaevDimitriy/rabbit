import pytest
from web.aio_server import init_app
from web_core.defaults import CSRF_TOKEN_NAME


@pytest.fixture
def cli(loop, test_client):
    app = loop.run_until_complete(init_app(loop=loop))
    return loop.run_until_complete(test_client(app))


@pytest.fixture
def login_data():
    return {'login': 'shurup@mail.mail', 'password': '12345678', 'next': '/'}


@pytest.fixture
def login_data_no_customer():
    return {'login': 'bolt@mail.mail', 'password': '12345678', 'next': '/'}


@pytest.fixture()
def logged_cli(request, loop, cli, login_data):
    async def async_login(client, user_data):
        resp = await client.get('/login')
        csrf = resp.cookies[CSRF_TOKEN_NAME]
        data = {key: val for key, val in user_data.items()}
        data[CSRF_TOKEN_NAME] = csrf.value
        await client.post('/login', data=data)

    loop.run_until_complete(async_login(cli, login_data))

    def logout():
        async def async_logout(client):
            resp = await client.get('/')
            csrf = resp.cookies[CSRF_TOKEN_NAME]
            await client.post('/logout', data={'next': '/', CSRF_TOKEN_NAME: csrf.value})

        loop.run_until_complete(async_logout(cli))

    request.addfinalizer(logout)

    return cli

@pytest.fixture()
def logged_cli_no_customer(request, loop, cli, login_data_no_customer):
    async def async_login(client, user_data):
        resp = await client.get('/login')
        csrf = resp.cookies[CSRF_TOKEN_NAME]
        data = {key: val for key, val in user_data.items()}
        data[CSRF_TOKEN_NAME] = csrf.value
        await client.post('/login', data=data)

    loop.run_until_complete(async_login(cli, login_data_no_customer))

    def logout():
        async def async_logout(client):
            resp = await client.get('/')
            csrf = resp.cookies[CSRF_TOKEN_NAME]
            await client.post('/logout', data={'next': '/', CSRF_TOKEN_NAME: csrf.value})

        loop.run_until_complete(async_logout(cli))

    request.addfinalizer(logout)

    return cli
