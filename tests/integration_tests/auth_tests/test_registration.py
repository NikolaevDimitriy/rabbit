__author__ = 'dimitriy'
import asyncio
import ujson as json
from datetime import datetime

import pytest
from aiopg.sa import create_engine
from aioredis import create_pool

import settings as ls
from auth.model import AuthUser
from auth.register import UserRegistration


@pytest.fixture
def user_form():
    return {
        'phone': '+7 905 273 56 99',
        'email': 'test_mail@mail.mail',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name'
    }






