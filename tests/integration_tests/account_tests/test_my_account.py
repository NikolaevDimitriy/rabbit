__author__ = 'dimitriy'

import pytest
from web_core.defaults import CSRF_TOKEN_NAME
from models.account.user import User
import json


@pytest.mark.run_loop
async def test_get_form(cli):
    resp = await cli.get('/login')
    assert resp.status == 200


@pytest.mark.run_loop
async def test_login(cli, login_data):
    resp = await cli.get('/login')
    csrf = resp.cookies[CSRF_TOKEN_NAME]
    data = {key: val for key, val in login_data.items()}
    data[CSRF_TOKEN_NAME] = csrf.value
    resp = await cli.post('/login', data=data)
    assert resp.status == 200
    assert resp.cookies.get('rabbit', None) is not None

    csrf = resp.cookies[CSRF_TOKEN_NAME]
    resp = await cli.post('/logout', data={'next': '/', CSRF_TOKEN_NAME: csrf.value})
    assert resp.status == 200


@pytest.mark.run_loop
async def test_get_my_account_data(logged_cli, login_data):
    resp = await logged_cli.get('api/my_account')
    text = await  resp.text()
    print(text)
    my_account = json.loads(text)
    print(my_account)
    assert resp.status == 200
    assert my_account is not None
    user = User().from_dict(**my_account)
    assert login_data['login'] == user.email
    assert user.pwd is None


async def test_update_my_account_data_success(logged_cli, new_account_data):
    resp = await logged_cli.post('api/my_account', data=json.dumps(new_account_data[0]))
    new_account = await resp.json()
    assert resp.status == 200
    assert new_account['success']
    assert new_account['instance']['email'] == new_account_data[0]['email']
    assert new_account['instance']['phone'] == new_account_data[0]['phone']
    assert new_account['instance']['first_name'] == new_account_data[0]['first_name']
    assert new_account['instance']['last_name'] == new_account_data[0]['last_name']
    assert new_account['instance']['timezone'] == new_account_data[0]['timezone']
    assert new_account['instance']['role'] == new_account_data[0]['role']

    resp = await logged_cli.post('api/my_account', data=json.dumps(new_account_data[1]))
    new_account = await resp.json()
    assert resp.status == 200
    assert new_account['success']
    assert new_account['instance']['email'] == new_account_data[1]['email']
    assert new_account['instance']['phone'] == new_account_data[1]['phone']
    assert new_account['instance']['first_name'] == new_account_data[1]['first_name']
    assert new_account['instance']['last_name'] == new_account_data[1]['last_name']
    assert new_account['instance']['timezone'] == new_account_data[1]['timezone']
    assert new_account['instance']['role'] == new_account_data[1]['role']


