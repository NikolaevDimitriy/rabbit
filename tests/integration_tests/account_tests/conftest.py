__author__ = 'dimitriy'

import pytest


@pytest.fixture
def new_account_data():
    return (
        {
            'email': 'mail@mail.mail',
            'phone': '+79052735697',
            'first_name': 'test_first_name',
            'last_name': 'test_last_name',
            'timezone': 'Europe/Moscow',
            'role': 1
        }, {
            'email': 'shurup@mail.mail',
            'phone': '+79052439987',
            'first_name': 'Shura',
            'last_name': 'Shurupov',
            'timezone': 'Europe/Moscow',
            'role': 1
        }
    )
