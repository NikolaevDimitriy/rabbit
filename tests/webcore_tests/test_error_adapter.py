__author__ = 'dimitriy'


from web_core.validators.json_schema import Draft4ErrorAdapter, map_of_validators
from jsonschema import Draft4Validator, FormatChecker


def test_multiple_error_adapter(invalid_m_data):
    adapter = Draft4ErrorAdapter(map_of_validators)
    for data in invalid_m_data:
        Draft4Validator.check_schema(data['schema'])
        validator = Draft4Validator(schema=data['schema'], format_checker=FormatChecker())

        errors = adapter.adapt_errors(validator.iter_errors(data['data']))
        assert len(errors) == len(data['expectation'])
        data['expectation'].sort(key=lambda x: x.identity)
        errors.sort(key=lambda x: x.identity)
        for t in zip(data['expectation'], errors):
            assert t[0].identity == t[1].identity
            assert t[0].errors[0] == t[1].errors[0] or t[0].errors[1] == t[1].errors[0]


def test_single_error_adapter(invalid_data):
    adapter = Draft4ErrorAdapter(map_of_validators)
    for data in invalid_data:
        Draft4Validator.check_schema(data['schema'])
        validator = Draft4Validator(data['schema'], format_checker=FormatChecker())
        errors = adapter.adapt_errors(validator.iter_errors(data['data']))
        print(data['expectation'])
        assert errors[0].identity == data['expectation'].identity
        assert errors[0].errors[0] == data['expectation'].errors[0]






