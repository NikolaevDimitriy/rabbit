__author__ = 'dimitriy'
import pytest
from utils.types.return_types import ErrorResult


@pytest.fixture
def invalid_data():
    return [{
        'schema': {
            'type': 'object',
            'properties': {
                'selection': {'enum': ['1', '2', '3']}
            }
        },
        'data': {
            'selection': '4'
        },
        'expectation': ErrorResult(identity='selection', errors=['Value is not a part of given set.'])
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'value': {'type': 'integer'}
            }
        },
        'data': {
            'value': '4'
        },
        'expectation': ErrorResult(identity='value', errors=['Invalid type.'])
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'value': {'type': 'integer'},
                'val': {'type': 'integer'}
            },
            'required': ['value']

        },
        'data': {
            'val': 2
        },
        'expectation': ErrorResult(identity='_all_', errors=['Missing required data.'])
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'value': {'type': 'integer'},
            },
            'additionalProperties': False

        },
        'data': {
            'value': 2,
            'val': 2
        },
        'expectation': ErrorResult(identity='_all_', errors=['Additional data are not allowed.'])
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'value': {
                    'type': 'object',
                    'properties': {
                        'val': {'type': 'integer'}
                    },
                    'additionalProperties': False
                },
            },
            'additionalProperties': False

        },
        'data': {
            'value': {
                'val': 2,
                'vl': 1
            }
        },
        'expectation': ErrorResult(identity='value', errors=['Additional data are not allowed.'])
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'value': {
                    'type': 'object',
                    'properties': {
                        'inner_value': {'type': 'string', 'minLength': 4}
                    }
                },
            },
        },
        'data': {
            'value': {
                'inner_value': '12'
            }
        },
        'expectation': ErrorResult(identity='value/inner_value', errors=['Value is to short.'])
    }]


@pytest.fixture
def invalid_m_data():
    return [{
        'schema': {
            'type': 'object',
            'properties': {
                'email': {'type': 'string'},
                'phone': {'type': 'string'}
            },
            'anyOf': [
                {'properties': {
                    'email': {
                        'format': 'email',
                    }
                }},
                {'properties': {
                    'phone': {
                        'pattern': '^((8|\+7)[-\s]?)?(\(?\d{3}\)?[-\s]?)?[-\d\s]{7,10}$',
                    }
                }},
            ],
        },
        'data': {
            'email': 'sdfsdkjhTsdf.sdf',
            'phone': 'dgdf'
        },
        'expectation': [ErrorResult(identity='email', errors=['Value does not match to validation pattern.', '']),
                        ErrorResult(identity='phone', errors=['Value does not match to validation pattern.', '']),
                        ErrorResult(identity='_all_', errors=['Any of fields are required (email or phone).',
                                                              'Any of fields are required (phone or email).'])]
    }, {
        'schema': {
            'type': 'object',
            'properties': {
                'user': {
                    'type': 'object'
                },
                'user1': {
                    'type': 'object'
                }
            },
            'anyOf': [
                {'properties': {
                    'user': {
                        'type': 'object',
                        'properties': {
                            'email': {'type': 'string'},
                            'phone': {'type': 'string'}
                        }
                    }
                }},
                {'properties': {
                    'user1': {
                        'type': 'object',
                        'properties': {
                            'email': {'type': 'string'},
                            'phone': {'type': 'string'}
                        },
                        'anyOf': [
                            {'properties': {
                                'email': {
                                    'format': 'email',
                                }
                            }},
                            {'properties': {
                                'phone': {
                                    'pattern': '^((8|\+7)[-\s]?)?(\(?\d{3}\)?[-\s]?)?[-\d\s]{7,10}$',
                                }
                            }},
                        ]
                    }
                }},
            ]

        },
        'data': {
            'user': {
                'email': 0,
                'phone': 1
            },
            'user1': {
                'email': 'emailTmail.mail',
                'phone': 'sfs'
            },
        },
        'expectation': [ErrorResult(identity='user/email', errors=['Invalid type.', '']),
                        ErrorResult(identity='user/phone', errors=['Invalid type.', '']),
                        ErrorResult(identity='user1/email', errors=['Value does not match to validation pattern.', '']),
                        ErrorResult(identity='user1/phone', errors=['Value does not match to validation pattern.', '']),
                        ErrorResult(identity='user1', errors=['Any of fields are required (email or phone).',
                                                              'Any of fields are required (phone or email).']),
                        ErrorResult(identity='_all_', errors=['Any of fields are required (user or user1).',
                                                              'Any of fields are required (user1 or user).'])]
    }]