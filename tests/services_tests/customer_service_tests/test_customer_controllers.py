__author__ = 'amicus'

from services.customer.controllers import IndividualController, LegalEntityController


def test_customer_controller_individual():
    individual_customer_controller = IndividualController()
    res = individual_customer_controller.update()
    assert res.success


def test_customer_controller_legal():
    legal_customer_controller = LegalEntityController()
    res = legal_customer_controller.update()
    assert res.success
