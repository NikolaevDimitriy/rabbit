__author__ = 'dimitriy'

from services.customer.validators import IndividualValidator, LegalEntityValidator


def test_validation_customer_valid(individual_valid):
    for json in individual_valid:
        validator = IndividualValidator(data=json)
        errors = validator.validate()
        assert not errors


def test_validation_customer_invalid(individual_invalid):
    for json in individual_invalid:
        validator = IndividualValidator(data=json)
        errors = validator.validate()
        assert errors


def test_validation_legalentity_valid(legalentity_valid):
    for json in legalentity_valid:
        validator = LegalEntityValidator(data=json)
        errors = validator.validate()
        assert not errors


def test_validation_legalentity_invalid(legalentity_invalid):
    for json in legalentity_invalid:
        validator = LegalEntityValidator(data=json)
        errors = validator.validate()
        assert errors
