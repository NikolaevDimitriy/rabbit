__author__ = 'dimitriy'
import pytest

@pytest.fixture
def individual_valid():
    return [{
        'type': 2,
        'phone': '+7 905 273 56 99',
        'requisites': {
            'passport_id': '3434 783423',
            'issue_date': '24.02.2010',
            'first_name': 'Shura',
            'patronymic': 'Shurikovich',
            'last_name': 'Shurupov',
            'birth_date': '17.01.1999',
            'sex': True
        },
        'actual_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543

        },
        'legal_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        }
    }
    ]


@pytest.fixture
def individual_invalid():
    return [{
        'type': 1,
        'phone': '+7 905 273 56 99',
        'requisites': {
            'passport_id': '3434 783423',
            'issue_date': '24.05.2010',
            'first_name': 'Shura',
            'patronymic': 'Shurikovich',
            'last_name': 'Shurupov',
            'birth_date': '17.01.1999',
            'sex': True
        },
        'actual_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        },
        'legal_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        }
    }
    ]

@pytest.fixture
def legalentity_valid():
    return [{
        'type': 1,
        'phone': '+7 905 273 56 99',
        'requisites': {

        },
        'actual_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543

        },
        'legal_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        }
    }
    ]


@pytest.fixture
def legalentity_invalid():
    return [{
        'type': 2,
        'phone': '+7 905 273 56 99',
        'requisites': {

        },
        'actual_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        },
        'legal_address': {
            'country_id': 1,
            'country_name': 'Страна',
            'region_id': 2,
            'region_name': 'Регион',
            'district_id': 3,
            'district_name': 'Район',
            'city_id': 4,
            'city_name': 'Город',
            'street': 'Улица',
            'building': '4/к',
            'apartment': '234',
            'underground_station_id': 34,
            'underground_station_name': 'Метро',
            'latitude': 34.565643,
            'longitude': 56.454543
        }
    }
    ]