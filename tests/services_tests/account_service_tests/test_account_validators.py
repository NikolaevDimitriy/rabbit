__author__ = 'dimitriy'

from services.account.validators import MyAccountJsonValidator


def test_validation_account_valid(users_json_valid):
    for user_json in users_json_valid:
        validator = MyAccountJsonValidator(data=user_json)
        errors = validator.validate()
        assert not errors


def test_validation_account_invalid(users_json_invalid):
    for user_json in users_json_invalid:
        validator = MyAccountJsonValidator(data=user_json)
        errors = validator.validate()
        print(user_json)
        assert errors
