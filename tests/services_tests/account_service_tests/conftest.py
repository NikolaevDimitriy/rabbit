__author__ = 'dimitriy'
import pytest


@pytest.fixture
def users_json_valid():
    return [{
        'email': 'mail@mail.ru',
        'phone': '+79052735699',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': 'Europe/Moscow',
        'role': 1
    }, {
        'email': 'mail@mail.ru',
        'phone': '',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': 'Europe/Moscow',
        'role': 1
    }, {
        'email': '',
        'phone': '+79052735699',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': 'Europe/Moscow',
        'role': 1
    }
    ]


@pytest.fixture
def users_json_invalid():
    return [{
        'email': '',
        'phone': '',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': 'Europe/Moscow',
        'role': 1
    }, {
        'email': 'mail@mail.ru',
        'phone': '',
        'first_name': '',
        'last_name': 'test_last_name',
        'timezone': 'Europe/Moscow',
        'role': 1
    }, {
        'email': 'mailTmail.ru',
        'phone': '',
        'first_name': 'test_first_name',
        'last_name': '',
        'timezone': 'Europe/Moscow',
        'role': 1
    }, {
        'email': 'mail@mail.ru',
        'phone': '',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': '',
        'role': 1
    }, {
        'email': 'mail@mail.ru',
        'phone': '',
        'first_name': 'test_first_name',
        'last_name': 'test_last_name',
        'timezone': '',
        'role': ''
    }, {
        'email': 'mail@mail.ru',
        'phone': '',
        'timezone': '',
        'role': '',
        'pwd': '343534'
    }

    ]