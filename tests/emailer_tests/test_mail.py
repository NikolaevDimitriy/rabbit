__author__ = 'dimitriy'
import pytest
from email.mime.text import MIMEText
from email.header import Header
from emailer import aiosmtplib
from emailer.mail_sender import MailSender
from emailer.queue_msg import QMailMessage
from emailer.defaults import MailType
import jinja2
from jinja2 import PackageLoader
from babel.support import Translations


@pytest.fixture
def message():
    msg = MIMEText("Hi!\nКак дела?\nHere is the link you wanted:\nhttps://www.python.org", 'plain', 'utf-8')
    msg['Subject'] = Header("Link", charset='utf-8')
    msg['From'] = "constructor3@yandex.ru"
    msg['To'] = "constructor3@yandex.ru"
    return msg


@pytest.mark.run_loop
async def test_send(loop, message, smtp_server):
    smtp = aiosmtplib.SMTP(hostname='localhost', port=1025, loop=loop)
    await smtp.ready
    snd = MailSender(smtp, loop)

    res = await snd.send(message)
    assert not res


def test_template():
    template_env = jinja2.Environment(loader=PackageLoader('emailer'), extensions=['jinja2.ext.i18n'])
    template_env.install_gettext_translations(Translations())

    message = QMailMessage(template_env, mtype=MailType.html, ad_from='admin@outstaff.ru', ad_to='admin@outstaff.ru',
                           subject='Privet', context={
                                'user_name': 'Tester',
                                'approval_link': 'http://localhost:8888/approve/1/7687687'
                            }, template_name='registration.html')

    text = message.get_text()

    assert text.startswith('<!DOCTYPE')

