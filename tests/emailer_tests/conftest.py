__author__ = 'dimitriy'
import pytest
import multiprocessing as mp
import smtpd
import asyncore
import time


@pytest.fixture(scope='module')
def smtp_server(request):

    def start_smtp():
        server = smtpd.DebuggingServer(('localhost', 1025), None)
        asyncore.loop()

    p = mp.Process(target=start_smtp, args=())
    p.start()
    time.sleep(1)
    print('server start')

    def fin():
        p.terminate()

    request.addfinalizer(fin)
    return p
