__author__ = 'dimitriy'

import pytest
import asyncio
import ujson as json
from amqp_core.protocol import connect
from emailer.publisher import EmailPublisher
from emailer.mail_sender import MailQueueWorker
from emailer.queue_msg import QMailMessage
from emailer import aiosmtplib
from emailer.defaults import MailType
import settings as ls
from emailer.mail_sender import MailSender
from utils.localization import gettext_local as _
msg_count = 0

TEST_MAIL_QUEUE_NAME = "test_mail_queue"


def test_qmail_json():
    msg = QMailMessage(mtype=MailType.plain, ad_from=ls.SENDER_EMAIL_ADDRESS, ad_to='receiver@mail.mail',
                       subject=_('Hello'), message='Тестовый message')
    str_data = msg.get_package()
    loaded = QMailMessage(**json.loads(str_data))

    assert msg._mtype == loaded._mtype
    assert msg._ad_from == loaded._ad_from
    assert msg._ad_to == loaded._ad_to
    assert msg._subject == loaded._subject
    assert msg._message == loaded._message
    assert list(msg._attachments) == list(loaded._attachments)


@pytest.mark.run_loop
async def test_pub_rec_msg(loop, smtp_server):
    transport, protocol = await connect(loop)
    publisher = EmailPublisher(transport, protocol, loop)
    publisher._queue_name = TEST_MAIL_QUEUE_NAME
    await publisher.connect()
    await publisher.declare_queue()

    smtp = aiosmtplib.SMTP(hostname='localhost', port=1025, loop=loop)
    await smtp.ready

    class FakeSender(MailSender):
        async def send(self, message):
            global msg_count
            msg_count += 1

    MailQueueWorker.sender_class = FakeSender
    worker = MailQueueWorker(smtp, transport=transport, protocol=protocol, loop=loop)

    worker._queue_name = TEST_MAIL_QUEUE_NAME
    await worker.connect()
    await worker.subscribe()

    tasks = [
        publisher.send_message(MailType.plain, ls.SENDER_EMAIL_ADDRESS, 'receiver@mail.mail', 'Hi', 'мессага'),
        publisher.send_message(MailType.html, ls.SENDER_EMAIL_ADDRESS, 'receiver@mail.mail', 'Privet', 'ну чего не отвечаешь'),
        publisher.send_message(MailType.plain, ls.SENDER_EMAIL_ADDRESS, 'receiver@mail.mail', 'Bye', 'давай досвидания'),
    ]

    done = await asyncio.wait(tasks, loop=loop, return_when=asyncio.ALL_COMPLETED)
    # await publisher.send_message(MailType.plain, ls.SENDER_EMAIL_ADDRESS, 'receiver@mail.mail', 'Привет', 'Тестовый message')

    while msg_count < len(tasks):
        await asyncio.sleep(1, loop=loop)

    await protocol.close()
    transport.close()
    assert msg_count == 3

