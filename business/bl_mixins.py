__author__ = 'dimitriy'

from storages import DataProviders
from business import BusinessValidationBase
from utils.exceptions import ImproperlyConfigured
from utils.types.return_types import SIEResult


class ValidationMixin(object):

    validator = BusinessValidationBase

    async def validate(self, user, instance, data_providers: DataProviders, **kwargs):
        if self.validator is not None:
            validator = self.validator(user=user, instance=instance, data_providers=data_providers, **kwargs)
            res = await validator.validate()
            return SIEResult(res, instance, validator.get_errors())
        raise ImproperlyConfigured("s% requires either a definition of 'validator'" % self.__class__.__name__)
