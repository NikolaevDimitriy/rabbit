__author__ = 'dimitriy'

from storages.customer import CustomerStorage, CustomerUniqueChecker
from storages.utility_storage import Condition
from business import BusinessValidationBase
from business.bl_base import Saver
from utils.localization import gettext_local as _
from utils.types.return_types import ErrorResult
from models.customer.customer import Customer, LegalEntityRequisitesNames as _lenames, IndividualRequisitesNames as _inemes
from sqlalchemy.types import Unicode
from business.customer.ref_user_updater import RefUserUpdater

class CustomerValidator(BusinessValidationBase):
    async def validate(self):

        if self._instance.type == 0:
            self._errors.append(ErrorResult('type', str(_('Type can not be undefined.'))))
            return False

        res = await self.check_unique()
        for k, v in res.items():
            if v:
                self._errors.append(ErrorResult(k, str(_('s% already exists.')) % k))
        if self._errors:
            return False
        return True

    async def check_unique(self):
        checker = CustomerUniqueChecker(self._data_providers)
        conditions = self.get_unique_check_conditions()
        if conditions:
            res = await checker.check_exists(conditions)
            return res
        return {}

    def get_unique_check_conditions(self):
        if self._instance.type == 1:  # legal entity
            return [Condition(
                _lenames.inn,
                [Customer.requisites[_lenames.inn].cast(Unicode) == self._instance.requisites.get(_lenames.inn, ''),
                 Customer.tbc.type == self._instance.type, Customer.tbc.id != self._instance.id]
            )]
        elif self._instance.type == 2:  # individual
            return [Condition(
                _inemes.passport_id,
                [Customer.requisites[_inemes.passport_id].cast(Unicode) == self._instance.requisites.get(_inemes.passport_id, ''),
                 Customer.tbc.type == self._instance.type, Customer.tbc.id != self._instance.id]
            )]


class MyCustomer(Saver):
    validator = CustomerValidator
    storage = CustomerStorage

    def get_fields_for_saving(self, existing):
        if existing:
            res = []
            if self._instance.name != existing.name:
                res.append(Customer.name)
            if self._instance.phone != existing.phone:
                res.append(Customer.phone)
            if self._instance.type != existing.type:
                res.append(Customer.type)
            if self.are_requisites_changed(existing.requisites):
                res.append(Customer.requisites)
            if len(res) > 0:
                return res
            return None
        return self.fields

    async def get_existing(self, storage):
        existing = await storage.get(self._instance.id)
        return existing

    def are_requisites_changed(self, old_requisites):
        if old_requisites is None and self._instance.requisites is not None:
            return True
        new_req_keys = sorted(self._instance.requisites.keys())
        old_req_keys = sorted(old_requisites.keys())
        res = False
        for new, old in zip(new_req_keys, old_req_keys):
            if new != old or self._instance.requisites.get(new) != old_requisites.get(old):
                res = True
                break
        return res

    async def on_saved_instance(self):
        user_updater = RefUserUpdater(self._user, self._instance, self._data_providers)
        await user_updater.update_customer_users()
