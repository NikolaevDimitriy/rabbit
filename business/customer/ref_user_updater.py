__author__ = 'dimitriy'
from business import BusinessLogicBase
from storages.account import UserStorage
from models.account.user import User


class RefUserUpdater(BusinessLogicBase):

    async def update_customer_users(self):
        storage = UserStorage(self._data_providers)
        if self._instance.type == 1:
            customer_users = await storage.get_list(conditions=[User.customer_id == self._instance.id])
            if customer_users:
                for user in customer_users:  # todo bulk save use unnest
                    await self.assign_customer(user, storage)
            else:
                await self.assign_customer(self._user, storage)
        else:
            await self.assign_customer(self._user, storage)

    async def assign_customer(self, user, storage):
        only_redis = user.customer_id == self._instance.id
        user.customer_prop = self._instance
        if only_redis:
            await storage.save_to_redis(user)
        else:
            await storage.save(user, fields=(User.customer_id,))
