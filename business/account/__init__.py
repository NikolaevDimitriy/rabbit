__author__ = 'dimitriy'
from storages.account import UserStorage
from storages.account.additional import UserUniqueChecker
from storages.utility_storage import Condition
from business import BusinessValidationBase
from business.bl_base import Saver
from utils.localization import gettext_local as _
from utils.types.return_types import ErrorResult
from models.account.user import User


class AccountValidator(BusinessValidationBase):

    async def validate(self):
        res = await self.check_unique_email_phone()
        for k, v in res.items():
            if v:
                self._errors.append(ErrorResult(k, str(_('s% already exists')) % k))
        if self._errors:
            return False
        return True

    async def check_unique_email_phone(self):
        checker = UserUniqueChecker(self._data_providers)
        res = await checker.check_exists([Condition(User.email.name, [User.tbc.email == self._instance.email,
                                                                      User.tbc.id != self._instance.id]),
                                          Condition(User.phone.name, [User.tbc.phone == self._instance.phone,
                                                                      User.tbc.id != self._instance.id])])
        return res


class MyAccount(Saver):
    validator = AccountValidator
    storage = UserStorage
    fields = (User.email, User.phone, User.first_name, User.last_name, User.timezone, User.role)

    async def get_existing(self, storage):
        return self._user

