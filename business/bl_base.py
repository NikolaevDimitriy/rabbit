__author__ = 'dimitriy'
from utils.exceptions import ImproperlyConfigured
from utils.types.return_types import SIEResult, ErrorResult
from business import BusinessLogicBase
from business.bl_mixins import ValidationMixin
from utils.localization import gettext_local as _


class Saver(ValidationMixin, BusinessLogicBase):
    storage = None
    fields = ()

    async def save(self) -> SIEResult:
        res = await self.validate(self._user, self._instance, self._data_providers)
        if res.success:
            saver = self.get_storage()
            existing = await self.get_existing(saver)
            fields_to_save = self.get_fields_for_saving(existing)
            if fields_to_save is not None:
                save_res = await saver.save(self._instance, fields_to_save)
                if save_res:
                    await self.on_saved_instance()
                    return res
            else:
                return SIEResult(False, self._instance, [ErrorResult('_all_', str(_('There are no changes to save.')))])
        return SIEResult(False, self._instance, res.errors)

    def get_storage(self):
        if self.storage is not None:
            return self.storage(self._data_providers)
        raise ImproperlyConfigured("Saver requires either a definition of 'storage'")

    def get_fields_for_saving(self, existing):
        return self.fields

    async def get_existing(self, storage):
        # existing = await storage.get(self._instance.id)
        # return existing
        return None
    async def on_saved_instance(self):
        pass

