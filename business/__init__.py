import abc

from storages import DataProviders
from utils.types.return_types import SIEResult

__author__ = 'dimitriy'


class BusinessLogicBase(metaclass=abc.ABCMeta):

    def __init__(self, user=None, instance=None, data_providers: DataProviders=None):
        self._user = user
        self._instance = instance
        self._data_providers = data_providers


class BusinessValidationBase(BusinessLogicBase):

    def __init__(self, user=None, instance=None, data_providers: DataProviders=None, **kwargs):
        super(BusinessValidationBase, self).__init__(user=user, instance=instance, data_providers=data_providers)
        self._errors = []

    @abc.abstractmethod
    async def validate(self):
        return True

    def get_errors(self):
        return self._errors
