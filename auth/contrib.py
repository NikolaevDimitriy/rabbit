__author__ = 'dimitriy'

from auth.forms import LoginForm
from aiohttp_session import SESSION_KEY


def get_login_form(request):
    return LoginForm(request.app, meta={'csrf_context': request[SESSION_KEY]})