__author__ = 'dimitriy'

from web_core.application import AIOApplication
from auth.views import AioLoginView, AioLogoutView, AioRegisterView, AioApproveView


class AioAuthApplication(AIOApplication):

    def name(self):
        return 'auth'

    def get_aio_urls(self):
        return (
            ('login', AioLoginView, 'login'),
            ('logout', AioLogoutView, 'logout'),
            ('register', AioRegisterView, 'register'),
            ('approve/{uid:\d+}/{approval_code}', AioApproveView, 'approve')
        )

aio_application = AioAuthApplication()
