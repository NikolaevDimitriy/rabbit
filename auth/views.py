__author__ = 'dimitriy'

from aiohttp.web import HTTPFound
from aiohttp_session import SESSION_KEY
from auth.auth_backend import AioAuthBackend
from auth.forms import RegistrationForm, LoginForm, LogoutForm
from auth.model import AuthUser
from auth.register import UserRegistration
from web_core.views import ProcessFormView, DetailView
from web_core.defaults import USER_SESSION_KEY
from storages.account import UserStorage
from utils.localization import gettext_local as _

class AioLoginView(ProcessFormView):
    template_name = 'auth/login.html'
    form_class = LoginForm
    page_identity = 'login'
    success_url = '/'

    async def form_valid(self, form, **kwargs):
        auth_backend = AioAuthBackend(self.request.app.data_providers)
        user = await auth_backend.authenticate(form.login.data, form.password.data)
        if user is not None and user.is_active:
            session = self.request[SESSION_KEY]
            session[USER_SESSION_KEY] = user.id
            self.request.user = user
            return await super(AioLoginView, self).form_valid(form, ** kwargs)
        elif user is None:
            form.out_errors = [_('Login does not exists or incorrect password')]
        elif not user.is_active:
            form.out_errors = [_('Account is not approved')]
        return await self.form_invalid(form, **kwargs)

    def get_success_url(self, form):
        return form.next.data


class AioLogoutView(ProcessFormView):
    template_name = 'auth/login.html'
    page_identity = 'logout'
    form_class = LogoutForm

    def get_success_url(self, form):
        return form.next.data

    async def form_valid(self, form, **kwargs):
        session = self.request[SESSION_KEY]
        session.invalidate()
        return await super(AioLogoutView, self).form_valid(form, ** kwargs)

    async def get(self, *args, **kwargs):
        session = self.request[SESSION_KEY]
        session.invalidate()
        return HTTPFound('/')


class AioRegisterView(ProcessFormView):
    template_name = 'auth/register.html'
    page_identity = 'registration'
    form_class = RegistrationForm
    success_url = '/'

    async def form_valid(self, form, **kwargs):
        user = AuthUser.get_default()
        form.populate_obj(user)
        logic = UserRegistration(user, self.request.app.data_providers,
                                 self.request.app.qmailer, self.request.app.router)
        await logic.register()
        return await super(AioRegisterView, self).form_valid(form, **kwargs)


class AioApproveView(DetailView):
    template_name = 'auth/approve_result.html'
    page_identity = 'approve'
    pk_url_kwarg = 'uid'
    obj_loader = UserStorage

    async def get_context_data(self, **kwargs):
        res = await super(AioApproveView, self).get_context_data(**kwargs)
        if not self.object.is_active:
            logic = UserRegistration(self.object, self.request.app.data_providers,
                                     self.request.app.qmailer, self.request.app.router)

            is_approved = await logic.approve_registration(self.request.match_info.get('approval_code', None))
            if is_approved:
                res['approval_msg'] = _('Account is successfully approved.')
            else:
                res['approval_msg'] = _('Account is not approved.')
        else:
            res['approval_msg'] = _('Account has been already approved.')
        return res

