__author__ = 'dimitriy'
import passlib.hash


hasher = passlib.hash.pbkdf2_sha512


def make_password(row_pasword):
    return hasher.encrypt(row_pasword, salt_size=32, rounds=8000)


def check_password(row_password, hash_h):
    return hasher.verify(row_password, hash_h)
