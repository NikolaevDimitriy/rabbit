__author__ = 'dimitriy'
import uuid
from auth.model import AuthUser
from utils.localization import gettext_local as _
from emailer.publisher import EmailPublisher
from emailer.defaults import MailType, TEMPLATE_INDEX
from storages.account import UserStorage
from storages import DataProviders
import settings as ls


def get_redis_approval_key(approval_code):
    return 'approval:%s' % (approval_code,)


class UserRegistration(object):
    storage = UserStorage

    def __init__(self, instance: AuthUser, data_providers: DataProviders, mail_publisher: EmailPublisher, router):
        self._instance = instance
        self._pg_db = data_providers.pg_db
        self._redis = data_providers.redis
        self._mail_publisher = mail_publisher
        self._router = router
        self._storage = self.storage(data_providers)

    async def register(self):
        res = await self._storage.save(instance=self._instance,
                                       fields=(AuthUser.email, AuthUser.phone,
                                               AuthUser.first_name, AuthUser.last_name,
                                               AuthUser.pwd, AuthUser.timezone),
                                       expire=ls.REGISTRATION_APPROVAL_PERIOD)
        if res:
            # generate and save approval code
            approval_code = await self.save_approval_to_redis()
            # send email with approval
            await self.send_approval_email(approval_code)

    async def approve_registration(self, approval_code):
        uid = await self.check_approval_code(approval_code)
        if uid == self._instance.id:
            self._instance.is_active = True
            await self._storage.save(self._instance, (AuthUser.is_active,))
            await self.delete_approval_code(approval_code)
            return True
        return False

    async def save_approval_to_redis(self):
        approval_code = uuid.uuid4().hex
        async with self._redis.get() as redis:
            await redis.set(get_redis_approval_key(approval_code), str(self._instance.id),
                            expire=ls.REGISTRATION_APPROVAL_PERIOD)
            return approval_code

    async def check_approval_code(self, approval_code):
        async with self._redis.get() as redis:
            uid = await redis.get(get_redis_approval_key(approval_code))
            if uid is not None:
                return int(uid)
            return None

    async def delete_approval_code(self, approval_code):
        async with self._redis.get() as redis:
            uid = await redis.delete(get_redis_approval_key(approval_code))
            if uid is not None:
                return int(uid)
            return None

    async def send_approval_email(self, approval_code):
        await self._mail_publisher.send_message(mtype=MailType.html, mail_to=self._instance.email,
                                                subject=_('Registration at'),
                                                context={
                                                    'user_name': self._instance.get_name(),
                                                    'approval_link': self._router['approve'].url(parts={
                                                        'uid': self._instance.id,
                                                        'approval_code': approval_code
                                                    })
                                                }, template_name=TEMPLATE_INDEX.get('register', None))
