__author__ = 'dimitriy'

from storages.account import UserStorage
from auth.model import AuthUser
from asyncio import CancelledError
from sqlalchemy import or_


class AuthUserStorage(UserStorage):
    model = AuthUser


class AioAuthBackend(object):
    storage = AuthUserStorage

    def __init__(self, data_providers):
        self._redis_pool = data_providers.redis
        self._db_engine = data_providers.pg_db
        self._storage = self.storage(data_providers)

    async def authenticate(self, username=None, password=None):
        try:
            user = await self._storage.get_single_from_sql(or_(AuthUser.tbc.email == username,
                                                               AuthUser.tbc.phone == username))
            if user is not None and user.check_password(password):
                return user
            return None
        except CancelledError:
            return None

    async def get_user(self, user_id):
        try:
            return await self._storage.get(user_id)
        except Exception:
            return None



