__author__ = 'dimitriy'
import asyncio
from wtforms.validators import ValidationError
from auth.model import AuthUser
import sqlalchemy as sa
import pytz


class Unique(object):
    def __init__(self, message=None):
        if not message:
            message = u'Field already exists.'
        self.message = message

    async def __call__(self, form, field):
        async with form.app.data_providers.pg_db.pg_slave.acquire() as conn:
            query = [sa.exists().where(AuthUser.tbc[field.short_name] == field.data)]
            res = await conn.scalar(sa.select(query))
            if res:
                raise ValidationError(self.message)
unique = Unique


class TZCheck(object):
    def __init__(self, message=None):
        if not message:
            message = u'Unknown timezone.'
        self.message = message

    def __call__(self, form, field):
        try:
            tz = pytz.timezone(field.data)
        except:
            raise ValidationError(self.message)

tzcheck = TZCheck
