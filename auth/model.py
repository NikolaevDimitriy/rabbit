__author__ = 'dimitriy'
from models.account.user import User
from auth.crypto import check_password, make_password


class AuthUser(User):
    __tablename__ = 'users'

    def __init__(self, **kwargs):
        super(AuthUser, self).__init__(**kwargs)
        self._password = None

    def set_password(self, raw_password):
        self.pwd = make_password(raw_password)
        self._password = raw_password

    def check_password(self, raw_password):
        return check_password(raw_password, self.pwd)
