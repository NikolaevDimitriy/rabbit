__author__ = 'dimitriy'
from wtforms import StringField, PasswordField, HiddenField, SelectField, validators as v
from utils.localization import gettext_local as _
from web_core.forms.forms import BaseForm
from auth.validators import Unique, TZCheck
from web_core.forms.fields import AsyncStringField
import pytz
from utils.datetime_helper import get_tz_offset_str


class RegistrationForm(BaseForm):
    email = AsyncStringField(_('Email'), validators=[v.Email(), v.Length(max=256), v.InputRequired(), Unique()])
    phone = AsyncStringField(_('Phone'), validators=[v.Length(max=12), Unique()])
    first_name = StringField(_('First name'), validators=[v.Length(max=64), v.InputRequired()])
    last_name = StringField(_('Last name'), validators=[v.Length(max=64), v.InputRequired()])
    password = PasswordField(_('Password'), validators=[v.Length(min=6, max=16), v.InputRequired()],
                             description=_('Min 8, max 16 symbols'))
    timezone = SelectField(_('Time zone'),
                           choices=[(tz, '%s (%s)' % (tz, get_tz_offset_str(tz),)) for tz in pytz.country_timezones('RU')],
                           validators=[v.InputRequired(), TZCheck()])
    next = HiddenField()

    def populate_obj(self, obj):
        obj.email = self.email.data
        obj.phone = self.phone.data
        obj.first_name = self.first_name.data
        obj.last_name = self.last_name.data
        obj.timezone = pytz.timezone(self.timezone.data)
        obj.set_password(self.password.data)


class LogoutForm(BaseForm):
    next = HiddenField(default='/')


class LoginForm(LogoutForm):
    login = StringField(_('Login'), validators=[v.InputRequired()])
    password = PasswordField(_('Password'), validators=[v.InputRequired()])
    next = HiddenField(default='/')





