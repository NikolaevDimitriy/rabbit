import os
import asyncio
from web.aio_server import init_server
import argparse

parser = argparse.ArgumentParser(description="aiohttp server")
parser.add_argument('port', type=int)

if __name__ == '__main__':

    print(os.getcwd())
    args = parser.parse_args()
    eloop = asyncio.get_event_loop()
    f = init_server(eloop, port=args.port)
    srv = eloop.run_until_complete(f)
    try:
        print('run server')
        eloop.run_forever()
    except KeyboardInterrupt:
        pass