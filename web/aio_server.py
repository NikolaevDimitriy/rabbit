__author__ = 'dimitriy'

import aiohttp_debugtoolbar
import settings as ls
from aiohttp.web import Application as AioApp, Response
from web_core.middlewares.session import get_session_middleware
from web_core.middlewares.auth import get_auth_middleware
from auth.auth_backend import AioAuthBackend
from amqp_core.protocol import connect
from emailer.publisher import EmailPublisher
from storages import create_data_providers
from web.aio_urls import init_urls, init_menu
from web.templating import setup_jinja2_env
from models import models

async def handler(request):
    return Response(text='ok')

async def init_app(loop):

    dp = await create_data_providers(loop)

    transport, protocol = await connect(loop)
    publisher = EmailPublisher(transport, protocol, loop)
    await publisher.connect()
    await publisher.declare_queue()

    session_middleware = get_session_middleware(dp.redis, ls.SESSION_REDIS_PREFIX, ls.SESSION_COOKIE_AGE)
    auth_middleware = get_auth_middleware(AioAuthBackend(dp))
    app = AioApp(loop=loop, middlewares=[session_middleware,
                                         auth_middleware], debug=ls.DEBUG)

    # install aiohttp_debugtoolbar
    if app.debug:
        aiohttp_debugtoolbar.setup(app, intercept_redirects=False)

    app.data_providers = dp

    # app.redis_pool = redis_pool
    # app.db_engine = db_engine
    # app.search_engine = search_engine
    app.qmailer = publisher

    init_urls(app.router)
    app.menu = init_menu()
    app.router.add_route("*", '/test', handler)
    setup_jinja2_env(app)
    app.on_cleanup.append(shutdown)
    return app

async def shutdown(app):
    app.data_providers.close()
    await app.data_providers.wait_closed()

async def init_server(loop, port=ls.PORT):
    app = await init_app(loop)
    server = await loop.create_server(app.make_handler(), ls.HOST, port)
    return server


