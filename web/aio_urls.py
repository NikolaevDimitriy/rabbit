__author__ = 'dimitriy'


from web_core.application import AIOApplication
from auth.app import aio_application as account_app
from promo.app import aio_application as promo_app
from services.account.app import aio_application as account_api
from services.customer.app import aio_application as customer_api


def init_flat_list(router, lst):
    for url in lst:
        if isinstance(url, list):
            init_flat_list(router, url)
        else:
            router.add_route('*', url[0], url[1], name=url[2])


def init_urls(router):
    init_flat_list(router, aio_application.aio_urls)


def init_menu():
    return aio_application.menu


class AioRabbit(AIOApplication):

    def name(self):
        return 'rabbit'

    promo_app = promo_app
    account_app = account_app
    account_api = account_api
    customer_api = customer_api

    def get_aio_urls(self):
        url_patterns = (
            AIOApplication.include('/', self.account_app.aio_urls),
            AIOApplication.include('/', self.promo_app.aio_urls),
            AIOApplication.include('/api/', self.account_api.aio_urls),
            AIOApplication.include('/api/', self.customer_api.aio_urls),
        )
        return url_patterns


aio_application = AioRabbit()
