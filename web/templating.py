__author__ = 'dimitriy'
from jinja2 import PrefixLoader, PackageLoader
import aiohttp_jinja2
from babel.support import Translations
from utils import jinja_extensions as jext


def get_template_loader():
    return PrefixLoader({
        'promo': PackageLoader('promo'),
        'auth': PackageLoader('auth'),
    })


def setup_jinja2_env(app):
    env = aiohttp_jinja2.setup(app, loader=get_template_loader(),
                               context_processors=[aiohttp_jinja2.request_processor],
                               extensions=['jinja2.ext.i18n'])
    env.install_gettext_translations(Translations())
    env.filters.update({
        'empty_if_none': jext.empty_if_none,
        'zero_if_none': jext.zero_if_none,
        'check_flag': jext.check_flag,
        'date': jext.format_datetime
    })



